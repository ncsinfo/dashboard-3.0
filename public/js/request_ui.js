//gen token form
$(document).ready(function(){
	
	$('#genToken').on('submit', function(){
		event.preventDefault();
		//Form Validation
		var inputRefcode = $("input[name='refCode']",this);
		var inputPayment = $("input[name='payfor']",this);
		var inputClientName = $("input[name='fullname']",this);
		var inputEmailAddress = $("input[name='email']",this);
		var inputPrice = $("input[name='figure']",this);
		var inputArr = [inputPrice,inputPayment,inputRefcode,inputPayment,inputClientName,inputEmailAddress];
		var pass = 0;
		for(var i = 0; i<inputArr.length;i++){
			if(inputArr[i].val() == null || inputArr[i].val() == ''){
				return inputArr[i].parent().addClass('has-error');
			}else{
				pass++;
			}
		}
		if(pass == inputArr.length){
			console.log('YEAH');
		}
		console.log('form submit ok');
		$.ajax({
			url:'/ajaxrequest',
			type:'POST',
			data:{
				requests:'tokengen',
				formDate:$(this).serialize()
			},
			success:function(d){
				/*$('#paymentLinkModal .modal-body').empty().append(d);*/
				$('#paymentLinkModal').modal();
				$('#holdText-0').css('border','none').val(d);
				$('#generatedlink').append(d);
				console.log(d);
			}
		})
	});
	
var formToken = $('#genToken');
var paymentLinkModal = $('#paymentLinkModal');
formToken.on('submit', function(){
	event.preventDefault();
	var inputRefcode = $("input[name='refCode']",this);
	var inputPayment = $("input[name='payfor']",this);
	var inputClientName = $("input[name='fullname']",this);
	var inputEmailAddress = $("input[name='email']",this);
	var inputPrice = $("input[name='figure']",this);
	var inputArr = [inputPrice,inputPayment,inputRefcode,inputPayment,inputClientName,inputEmailAddress];
	var pass = 0;
	for(var i = 0; i<inputArr.length;i++){
		if(inputArr[i].val() == null || inputArr[i].val() == ''){
			return inputArr[i].parent().addClass('has-error');
		}else{
			pass++;
		}
	}
	if(pass == inputArr.length){
		$.ajax({
			url:'/ajaxrequest',
			type:'POST',
			data:{
				requests:'tokengen',
				formDate:$(this).serialize()
			},
			success:function(d){
				/*$('#paymentLinkModal .modal-body').empty().append(d);*/
				$('#paymentLinkModal').modal();
				$('#holdText-0').css('border','none').val(d);
			}
		})
	}
	
})
// payment link modal after hidden
var paymentLinkStatTable = $('#PaymentLinksStatus tbody');
$('#paymentLinkModal').on('hidden.bs.modal', function () {
document.getElementById("genToken").reset();
 $.ajax({
			url:'/ajaxrequest',
			type:'POST',
			dataType:'JSON',
			data:{
				requests:'refreshlistofgeneratedlinks',
			},
			success:function(d){
				paymentLinkStatTable.empty();
				for(num in d){
					console.log(d[num].client_name);
				   paymentLinkStatTable.append('<tr id="'+d[num].tokenID+'"><td>'+d[num].client_name+'</td><td>'+d[num].trans_title+'</td><td id="token" class="'+d[num].token+'">'+d[num].token+'</td><td>'+d[num].price+'</td><td>'+d[num].attempts+'</td><td>'+d[num].status+'</td><td>'+d[num].date_create+'</td><td><i class="fa fa-trash payment-link-trash" aria-hidden="true" title="Delete"></i></td></tr>');
				}
			}
		})
})
//delete payment link
$(document).on('click','.payment-link-trash',function(){
	var parentTR = $(this).closest('tr');
	var tokenID = $(this).closest('tr').attr('id');
	console.log(tokenID);
	if(window.confirm("Do Want to delete this link ? ")){
		$.ajax({
			url:'/ajaxrequest',
				type:'POST',
				dataType:'JSON',
				data:{
					requests:'deletepaymentLink',
					token: tokenID
				},
				success:function(e){
					if(e){
					 parentTR.css('background-color','#ff0000').fadeOut('slow');
					}else{
						alert('Cannot Delete Record Please try again !!!');
					}
				}
		})

		;
	}else{
		console.log('canceled');
	}
	
})
// creating a new user 
var createForm = $('#create-new-user');
var newUserNotiProb = $('#newUserNotiProb');
var newUserNotiSuc = $('#newUserNotiSuc');
createForm.on('submit',function(){
    event.preventDefault();
      $.ajax({
      			url:'/ajaxrequest',
       			type:'POST',
				dataType:'JSON',
				data:{
					requests:'createnewuser',
					form: $(this).serialize(),
				},
				success:function(e){
					if(typeof e === 'object'){
						newUserNotiProb.show().empty(); // showing the notification box
						for(counter in e){
							if(typeof e[counter] === 'object'){
								for(c in e[counter]){
									newUserNotiProb.append("<p>"+e[counter][c]+"</p>");
								}
							}else{
								newUserNotiProb.append("<p>"+e[counter]+"</p>");
							}
						}
					}else{
						newUserNotiProb.css('display','none');
						newUserNotiSuc.show();
						createForm.trigger("reset");
					}
				}
      })
  })
});
// sort table on change date 
var tableClientbody0 = $('#clientTransactions tbody');
var dateSortfield = $('#dateSort');
$(document).on('change','#dateSort',function(){
	event.preventDefault();
	var date = $(this).val();
	var agent = $('#agent-ID').val();
	console.log(agent);
	
	$.ajax({
		url : '/ajaxrequest',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'dateSortAgentPage',
			dateField : date,
			sortAgent: agent,
            searchTxt: ''
				},
		success:function(e){
			console.log(date);
			console.log(agent);
		   if(e){
			   console.log(e);
		   	  tableClientbody0.empty();
		   	 for(counter in e){
			//	 console.log(agent);
		   	 	tableClientbody0.append('<tr><td>'+e[counter].onCard_name+'</td><td>'+e[counter].card_bank+'</td><td>'+e[counter].card_country+'</td><td>'+e[counter].status+'</td><td>'+e[counter].receiptNo+'</td><td>'+e[counter].client_name+'</td><td>'+e[counter].trans_title+'</td><td>'+(e[counter].currency === 'us' ? '$' : 'AED')+' '+e[counter].price+'</td><td>'+e[counter].ref_num+'</td><td>'+e[counter].client_ip+'</td><td>'+dateFormat(e[counter].trans_date)+'</td><tr>');
		   	 }
		   	 //console.log(e[counter].trans_date);
		   }
	   }
	})
})
// sort table on change date 
var tableClientbody = $('#clientTransactions tbody');
var sortAgent = $('#sortAgent');
$(document).on('change','#sortAgent',function(){
	event.preventDefault();
	var agent = $(this).val();
	var date = $('#dateSort').val();
	//console.log(agent);
	$.ajax({
		url : '/ajaxrequest',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'agentSortAgentPage',
			dateField : date,
			sortAgent: agent
				},
		success:function(e){
			//console.log('date');
			console.log(agent);
			console.log(e);
		   if(e){
		   	  tableClientbody.empty();
		   	 for(counter in e){
		   	 	tableClientbody.append('<tr><td>'+e[counter].onCard_name+'</td><td>'+e[counter].card_bank+'</td><td>'+e[counter].card_country+'</td><td>'+e[counter].status+'</td><td>'+e[counter].receiptNo+'</td><td>'+e[counter].client_name+'</td><td>'+e[counter].trans_title+'</td><td>'+(e[counter].currency === 'us' ? '$' : 'AED')+' '+e[counter].price+'</td><td>'+e[counter].ref_num+'</td><td>'+e[counter].client_ip+'</td><td>'+dateFormat(e[counter].trans_date)+'</td><tr>');
		   	 }
		   	 //console.log(e[counter].trans_date);
		   }
	   }
	})
})

$('.datepicker').datepicker({
    format: 'yyyy/mm/dd',
});
//View pop up link onclick token column
$(document).on('click','#token',function(){
	var tokenID = $(this).closest('tr').attr('id');
	var link = $(this).closest('td').attr('class');
	link = 'http://secure.southtravels.com/paynow.php?id=' + link;
	console.log(link);
	$('#paymentLinkGen').modal();
	$('#holdText').css('border','none').val(link);
	$('#tokenText').val(tokenID);
	console.log(tokenID);
});
//new email window on close payment link generated
var sendLink = $('#btn-copy-0');
sendLink.on('click',function() {
	var bodyContainer = $('#emailBody');
	var refNumber = $('#refCode').val();
	var linkVar0 = $('#holdText-0');
	var link = $('#holdText-0').val();
	var recipient = $('#client-email').val();
	var sub =  $('#payFor').val();
	var payee = $('#payee').val();
	var amount = $('#payAmount').val();
	var currency = $('#currency option:selected').val();
	var curr = $('select[id=currency]').val()
	var subBody = '';
	console.log(curr);
	linkVar0.select();
	document.execCommand('copy');
	$.ajax({
		url : 'send-email.php',
		type:'POST',
		data:{
			emailToken: 'new',
			emailRefNum: refNumber,
			emailAdd: recipient,
			emailLink: link,
			emailPayee: payee,
			emailAmount: amount,
			emailFor: sub,
			emailCurr: curr
		},
		success:function(e){
			console.log(e);
			bodyContainer.empty().append(e);
			if (e) {
				subBody = bodyContainer.text();
				parent.location = 'mailto:' + recipient + '?subject=' + refNumber + ' | ' + sub + '&body=' + subBody;
				bodyContainer.empty();
			}
		}
	})

});
//new email window on close payment link resend
var resendLink = $('#btn-copy');
resendLink.on('click',function() {
	var linkVar = $('#holdText');
	var tokenText = $('#tokenText').val();
	linkVar.select();
	document.execCommand('copy');
	console.log(tokenText);
	$.ajax({
		url : 'send-email.php',
		type:'POST',
		data:{
			emailToken: 'request',
			emailID: tokenText
		},
		success:function(e){
			console.log(e);
			parent.location = 'mailto:' + e;
		}
	})
});
//Date Sort Payment Link
var tableClientBody = $('#PaymentLinksStatus tbody');
var dateSField = $('#date-Sort');
dateSField.on('change',function(){
	var date = $(this).val();
	var agent = $('#agent-ID').val();
	console.log(date);
	console.log(agent);
	$.ajax({
		url : '/ajaxrequest',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'dateSortAgentPaymentLink',
			dateField : date,
			sortAgent: agent
		},
		success:function(e){
			if(e){
				tableClientBody.empty();
				for(counter in e){
					console.log(agent);
					tableClientBody.append('<tr id='+e[counter].tokenID+'><td>'+e[counter].client_name+'</td><td>'+e[counter].trans_title+'</td><td id="token" class="'+e[counter].token+'">'+e[counter].token+'</td><td>'+e[counter].price+'</td><td>'+e[counter].attempts+'</td><td>'+e[counter].status+'</td><td>'+dateFormat(e[counter].date_create)+'</td><td><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></td>');
				}
			}
		}
	})

});
/*Notification Area-------------------------------------------------------------*/
//Notification Search set every 30 seconds
var auto_refresh = setInterval(function ()
{
	loopSearch();
	loopSearch2();
}, 30000);
//Notification search
function loopSearch(){
	var agent = $('#agent-ID').val();
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		dataType: 'json',
		data: {
			requests: 'searchNotification',
			agentID: agent,
			typeNotify: 1
		},
		success: function (e) {
            console.log(e);
            console.log(agent);
			if (e) {
				if (e.length > 0) {
					notifyAgent(e,agent,'paymentLink');
				}
			}
			else {
				alert('Something went wrong.');
			}
		}
	});
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		dataType: 'json',
		data: {
			requests: 'searchNotification',
			agentID: agent,
			typeNotify: 2
		},
		success: function (e) {
			if (e) {
				if (e.length > 0) {
					notifyAgent(e,agent,'visa');
				}
			}
			else {
				alert('Something went wrong.');
			}
		}
	});
}
function loopSearch2(){
	var agent = $('#agent-ID').val();
	var cnt = 0;
	$('#notify-count-1').html('');
	$('#notify-count-2').html('0');
	$('#notify-body ul').empty();
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		dataType: 'json',
		data: {
			requests: 'searchNotification',
			agentID: agent,
			typeNotify: 4
		},
		success: function (e) {
			//console.log(agent);
			if (e.length > 0) {
				//console.log(e);
				$.each(e, function(index, element) {
					var timeLogged = timeExtension(element.trans_date,element.trans_time,'paymentLink');
					$('#notify-body ul').prepend('<li id="P'+element.id+'" class="notify-token"><a href="#"><h5><i class="fa fa-user text-light-blue"></i>&nbsp;'+element.client_name+'<small class="pull-right"><i class="fa fa-clock-o"></i>&nbsp;<span class="time-span">'+timeLogged+'</span></small></h5><p>'+element.trans_title+'</p></a></li>');
					cnt = cnt + 1;
				});
				$('#notify-count-1').html(cnt);
				$('#notify-count-2').html(cnt);
			}
		}
	});
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		dataType: 'json',
		data: {
			requests: 'searchNotification',
			agentID: agent,
			typeNotify: 3
		},
		success: function (e) {
			//console.log(agent);
			if (e.length > 0) {
				//console.log(agent);
				$.each(e, function(index, element) {
					var timeLogged = timeExtension(element.date_updated,element.date_updated,'visa');
					var visaType = element.visa_type + ' Days Visa';
					$('#notify-body ul').prepend('<li id="V'+element.id+'" class="notify-token"><a href="#"><h5><i class="fa fa-user text-light-blue"></i>&nbsp;'+element.name+'<small class="pull-right"><i class="fa fa-clock-o"></i>&nbsp;<span class="time-span">'+timeLogged+'</span></small></h5><p>'+visaType+'</p></a></li>');
					cnt = cnt + 1;
				});
				$('#notify-count-1').html(cnt);
				$('#notify-count-2').html(cnt);
			}
		}
	});
}
function timeExtension(transDate,transTime,type) {
	var timeLog = '';
	var timeMod = '';
	var oldDateTime = '';
	if (type=='visa') {
		oldDateTime = transDate;
	}else if (type=='paymentLink') {
		oldDateTime = transDate + ' ' + transTime;
	}
	var transDateTime = new Date(oldDateTime);
	var currDateTime = new Date();
	var timeWeek = '';
	var timeDays = '';
	var timeHours = '';
	var timeMinutes = '';
	var timeSeconds = '';
	//console.log(transDateTime.getTime());
	//console.log(currDateTime.getTime());
	var timeDiff = currDateTime.getTime() - transDateTime.getTime();
	timeDiff = (timeDiff / 1000);
	timeSeconds = timeDiff;
	timeMinutes = timeDiff / 60; //Convert time to minutes
	timeHours = timeDiff / 3600; //Convert time to hours
	timeDays = timeDiff / 86400; //Convert  time to days

	if (timeDiff < 60){
		timeLog = timeSeconds.toFixed() + ' sec';
		//console.log(timeLog);
	}
	else if (timeDiff < 3600 && timeDiff >= 60){
		timeMod = timeSeconds % 60;
		timeMinutes = Math.floor(timeMinutes);
		if (timeMinutes < 2) {
			timeLog = timeMinutes + ' min ' + timeMod.toFixed() + ' sec';
		}else {
			timeLog = timeMinutes + ' mins ' + timeMod.toFixed() + ' sec';
		}


		//console.log(timeLog);
	}
	else if (timeDiff < 86400 && timeDiff >= 3600){
		timeMod = timeMinutes % 60;
		timeMod = Math.floor(timeMod);
		timeHours = Math.floor(timeHours);
		//console.log('ok: ' + timeMod);
		if (timeHours < 2) {
			timeLog = timeHours + ' hr ' + timeMod + ' mins';
		}else {
			timeLog = timeHours + ' hrs ' + timeMod + ' mins';
		}
		//console.log(timeLog);
	}
	else if (timeDiff > 86400){
		timeDays = Math.floor(timeDays);
		if (timeDays < 2) {
			timeLog = 'Yesterday';
		}
		else if (timeDays > 1 && timeDays < 7){
			timeLog = timeDays + ' days ago';
		}
		else if (timeDays > 6 && timeDays < 14){
			timeLog = 'a week ago';
		}
		else if (timeDays > 13 && timeDays < 21){
			timeLog = '2 weeks ago';
		}
		else if (timeDays > 20 && timeDays < 28){
			timeLog = '3 weeks ago';
		}
		else if (timeDays >= 28 && timeDays < 31){
			timeLog = 'a month ago';
		}
		else {
			timeLog = 'more than 1 month ago';
		}
	}
	return timeLog;
	//console.log(timeLog);
}
//Delete notification on click
$(document).on('click','.notify-token',function(){
	var notifyID = $(this).closest('li').attr('id');
	var notificationCounter1 = $('#notify-count-1');
	var notificationCounter2 = $('#notify-count-2');
	var transType = notifyID.substr(0,1);
	var notifyID2 = notifyID.substr(1,notifyID.length-1);
	var notifyType = '';
	if (transType=='V'){notifyType=3;}
	else if (transType=='P'){notifyType=4;}
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		data: {
			requests: 'updateNotification',
			notifyID: notifyID2,
			typeNotify: notifyType
		},
		success: function (e) {
			console.log(notifyID2);
			var notifyIDDel = '#' + notifyID;
			$(notifyIDDel).remove();
			//console.log(notifyIDDel);
			var notifyCount = notificationCounter2.text();
			notifyCount = parseInt(notifyCount) - 1;
			notificationCounter2.text(notifyCount);
			if(notifyCount <= 0){notificationCounter1.text('');}
			else {notificationCounter1.text(notifyCount);}
			if (transType=='P'){
				window.location.href = 'client-transactions.php?searchID='+notifyID2;
			}
			else if (transType=='V'){
				window.location.href = 'your-uaevisa-transaction.php?visaID='+notifyID2;
			}

		}
	})
});
//Update Notification
function updateLoop(a) {
	console.log(a);
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		data: {
			requests: 'updateNotification',
			agentID: a,
			typeNotify: 1
		},
		success: function (e) {
			console.log(e);
		}
	})
}
function updateLoopVisa(a) {
	console.log(a);
	$.ajax({
		url: '/ajaxrequest',
		type: 'POST',
		data: {
			requests: 'updateNotification',
			agentID: a,
			typeNotify: 2
		},
		success: function (e) {
			console.log(e);
		}
	})
}
//Notification Pop up
var notificationEvents = ['onclose','onclick'];
function notifyAgent(x,y,z) {
	var title;
	var options;
	var strBody = '';
	if(z=='paymentLink'){
		$.each(x, function(index, element) {
			strBody = strBody + element.client_name + ': ' + element.trans_title +  '\n';
		});
	}else if(z=='visa'){
		$.each(x, function(index, element) {
			strBody = strBody + element.name + ': ' + element.visa_type +  ' Days Visa \n';
		});
	}

	title = 'Payment Link Notification';
	options = { body: strBody };
	Notification.requestPermission(function() {
		var notification = new Notification(title, options);
		notificationEvents.forEach(function(eventName) {
			notification[eventName] = function(event) {
				var c = event.type;
				if ((event.type=='click') || (event.type=='close')) {
					updateLoop(y);
					updateLoopVisa(y);
					console.log(c);
					notification.close();
				}
			};
		});
	});
}
//End Notification Area --------------------------------------------------------
//Pop up Client Details on Link Generation Form
var popDiv = $('#populateBody');
$(document).on('click','#pop',function(){
	var tokenID = $(this).closest('tr').attr('id');
	console.log(tokenID); //newly added
	$.ajax({
		url : '/ajaxrequest',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'popClientData',
			clientToken: tokenID
		},
		success:function(e){
			console.log(e);
			$('#popClientName').val(e[1]);
			$('#popClientEmail').val(e[3]);
			$('#popClientTrans').val(e[0]);
			$('#popClientAmount').val(e[2]);
			$('#popClientAmountCur').val(e[4]);
			$('#amount-paid').text(e[2] + ' ' + e[4]);
		}
	});
	$('#populateClient').modal();
	console.log(tokenID);
});
//Populate Client Details on Link Generation Form
$('#popLink').on('click',function(){
	$('#payee').val($('#popClientName').val());
	$('#client-email').val($('#popClientEmail').val());
	$('#payFor').val( $('#popClientTrans').val());
	$('#payAmount').val($('#popClientAmount').val());
});

// Format Date
function dateFormat(dateVal) {
	var monthNames = [
  "Jan", "Feb", "Mar",
  "Apr", "May", "Jun", "Jul",
  "Aug", "Sep", "Oct",
  "Nov", "Dec"
];
var date = new Date(dateVal);
var day = date.getDate(date);
var monthIndex = date.getMonth(date);
var year = date.getFullYear(date);
return monthNames[monthIndex]+' '+day+' '+year;
}

/*
Edit User Page buttons
--Edit Details
--Change Pass
--Add user capability
--Delete User
*/
// -- Change Pass -- //
var PassInput = $('#changePassInput');
var UpdatePassBut = $('#UpdatePass');
var UserID = '';
	//showing the modal
$(document).on('click','.change-pass',function(){
	UserID = $(this).closest('tr').attr('id');
	$('#change-pass-modal').modal();
});
	//start the user input check
PassInput.on('keyup',function(){
		var reg = /^(?=.*[A-Z]{1})(?=.*\d{1})(?=.*[!@#\$%\^&\*\(\)])(?=.*\w{8}).+$/i; // check of special char, a number , capital letter, 
		var searchRes = $(this).val().search(reg);
	if(searchRes >=0){
		UpdatePassBut.removeAttr('disabled');
	}else if(searchRes < 0 ){
		UpdatePassBut.attr('disabled','disabled');
	}
});
	//Submit the form
var changePassForm = $('#changePassForm');
changePassForm.on('submit',function(){
	event.preventDefault();
	$.ajax({
			url:'/ajaxrequest',
			type:'POST',
			data:{
				requests:'changePass',
				newpass: PassInput.val(),
				userid:UserID
			},
			success:function(d){
				if(d){
					UpdatePassBut.attr('disabled','disabled');
					$('#changePassInputCon').hide();
					$('#passChangeSuc').show();
					changePassForm.trigger("reset");
				}else{
					UpdatePassBut.attr('disabled','disabled');
					$('#changePassInputCon').hide();
					$('#passChangeErr').show();
					changePassForm.trigger("reset");
				}
			}
	})
})
 //closing the modal window
 $('#change-pass-modal').on('hidden.bs.modal', function (e) {
     $('#changePassInputCon').show();
	 $('#passChangeSuc').hide();
	 $('#passChangeErr').hide();
})
// -- end of Change Pass -- //

// -- Add user capability -- //
var addcapBut = $('.add-pri');

  //showing the modal window and fetching the user capabilities to display
addcapBut.on('click',function(){
	UserID = $(this).closest('tr').attr('id');
	$.ajax({
		url:'/ajaxrequest',
		type:'POST',
		dataType: 'JSON',
		data:{
			requests:'retUserCap',
			userid:UserID,
		},
		success:function(d){
			console.log(typeof d);
			if(d){
				console.log('sample inside d');
					$('#listofUserCap input').each(function(){
						if(d.hasOwnProperty($(this).attr('name'))){
							$(this).prop('checked',true);
							console.log('Yes');
						}
					})
			}else{
				console.log('sample');
				$('#listofUserCap input').each(function(){
					$(this).prop('checked',false);
				})
			}
		}
	});
	
	$('#userCapability').modal();
	
});
  //form submit
var userCapabilityForm = $('#userCapabilityForm');
userCapabilityForm.on('submit',function(){
	event.preventDefault();
	$.ajax({
		url:'/ajaxrequest',
		type:'POST',
		data:{
			requests:'addUserCap',
			cap: $(this).serialize(),
			userid:UserID,
		},
		success:function(e){
			if(e){
				$('#listofUserCap').hide();
				$('#addcapSuc').show();
			}else{
				$('#listofUserCap').hide();
				$('#addcapErr').show();
			}
		}
	})
})
$('#userCapability').on('hidden.bs.modal', function (e) {
     $('#listofUserCap').show();
	 $('#addcapSuc').hide();
	 $('#addcapErr').hide();
})

// -- end of  Add user capability -- //

// new method to add no whitespace on the begining of the field
jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.search(/^\s/g) < 0; 
}, "Invalid input");

//Add Visa Form
var addnewvisaForm = $("#add-new-visa");
var addvisaModal = $('#addvisaModal');
	addnewvisaForm.validate({
		rules:{
			passportNum:{
				required:true,
				noSpace:true,
			},
			visaDuration:{
				required:true,
			},
			ApplicantName:{
				required:true,
				noSpace:true,
			},
			agent_select:{
				required:true,
			},
			dateOfapplication:{
				required:true,
				noSpace:true,
			},
			agent_select:{
				required:true,
			},
			RequisitionNo:{
				required:true,
				noSpace:true,
			}

		},
		submitHandler:function(form){
			event.preventDefault();
			var agentID = $('#agent-ID').val();
			addvisaModal.modal();
			$.ajax({
				url:'/ajaxrequest',
				type:'POST',
				data:{
					requests:'addVisa',
					visaForm:$(form).serialize(),
				},
				success:function(e){
					if(e){
						$('#addvisapreloader').hide();
						$('#addVisaSuc').show();
						setInterval(function(){addvisaModal.modal('hide')},2000);
					}else{
						$('#addvisapreloader').hide();
						$('#addVisaSuc').show();
						setInterval(function(){addvisaModal.modal('hide')},2000);
					}
				}
			});
			$.ajax({
				url:'../controller/calendar.php',
				type:'POST',
				data:{
					requests:'addVisaToCalendar',
					agentID: agentID,
					formDetails:$(form).serialize()
				},
				success:function(e){
					if(e){
						console.log(e);
					}
				}
			});
			//$form.submit();
		}
	})
$('#addvisaModal').on('hidden.bs.modal', function (e) {
     $('#addvisapreloader').show();
	 $('#addVisaSuc').hide();
	 $('#addVisaSuc').hide();
	 addnewvisaForm.trigger("reset");
})
//End of add visa form

