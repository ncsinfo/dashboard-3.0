
//Check if their is a new message
var get_Msg = setInterval(function ()
{
		getRecentMsg();
}, 2000);	

function getRecentMsg() {
	var user = $("#user").val(); //user Name
	//var otherUser = $(".chat-user div").text(); //Recipient Username
	var otherUser = $("#rid").val();	
	var last = $(".chat-messages li:last").attr("id");
	var chatName = $(".chat-user div").text();
	var div = $('.direct-chat-messages'),
    height = div.height();
	
/*	//checkNewChat Message
	$.ajax({
		url:'../controller/chat.request.php',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'chatNotify',
			id: user,
		},
		success:function(e){
			

		}
	});*/
	
	if (otherUser){
	$.ajax({
		url:'../controller/chat.request.php',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'getChat',
			user: user,
			otherUser: otherUser,
		},
		success:function(e){
			//console.log(user);
			if(e){
				for( counter in e) {
					if(e[counter].id != last  ){
						
						if(otherUser != e[counter].user){
							$(".chat-messages").append('<li id="'+e[counter].id+'" class="'+ e[counter].user +'"><div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">ME</span><span class="direct-chat-timestamp pull-left">'+e[counter].date+'</span></div><div class="direct-chat-text">'+ e[counter].message +'</div></div></li>');
							height += div.height();
							div.animate({scrollTop: height}, 500);
						
						} else {
							$(".chat-messages").append('<li id="'+e[counter].id+'" class="'+ e[counter].user +'"><div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+ chatName +'</span><span class="direct-chat-timestamp pull-right">'+e[counter].date+'</span></div><div class="direct-chat-text">'+ e[counter].message +'</div></div></li>');
							height += div.height();
							div.animate({scrollTop: height}, 500);
							
						}
						
						//Update Status			
						$.ajax({
							url:'../controller/chat.request.php',
							type:'POST',
							dataType:'JSON',
							data:{
								requests:'updateStatus',
								id: user,
								senderid: otherUser,
								currid: e[counter].id,
							},
							success:function(e){
								
							}
						});
						
						
						
					}
				}
			}
		}
	});
		}
}


	
$(".prevlink").on("click", function(){
	var user = $("#user").val(); //user Name
	//var otherUser = $(".chat-user div").text(); //Recipient Username
	var otherUser = $("#rid").val();
	var last = $(".chat-messages li:first").attr("id");
	var chatName = $(".chat-user div").text();
	
	var div = $('.direct-chat-messages');
    height = div.height();
	console.log(last);
	$.ajax({
		url:'../controller/chat.request.php',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'getPreveMsg',
			user: user,
			otherUser: otherUser,
			fid: last,
		},
		success:function(e){	
			$('prevlink').hide();
			if(e){
				for( counter in e) {
					console.log(e[counter].user);
					
					if ( last >  e[counter].id) {
						if(otherUser != e[counter].user){
							$(".chat-messages").prepend('<li id="'+e[counter].id+'" class="'+ e[counter].user +'"><div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">ME</span><span class="direct-chat-timestamp pull-left">'+e[counter].date+'</span></div><div class="direct-chat-text">'+ e[counter].message +'</div></div></li>');
							
						} else {
							$(".chat-messages").prepend('<li id="'+e[counter].id+'" class="'+ e[counter].user +'"><div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+ chatName +'</span><span class="direct-chat-timestamp pull-right">'+e[counter].date+'</span></div><div class="direct-chat-text">'+ e[counter].message +'</div></div></li>');
							
						}
					} else {
						console.log('false');
					}
				}
			}
		}
	});
	
	
});


$(document).ready(function(){
	//Send Chat Message
	$("#chat-form").on('submit', function(event){
		event.preventDefault();
		
		var otherUser = $(".chat-user div").text();
		
		if(otherUser == "Messenger"){
			alert("Please select a user");
		} else{
		console.log(otherUser);
		var user = $("#user").val();
		var msg = $("#message").val();
		var rid = $("#rid").val();	
		$("#message").val("");
		$.ajax({
			url: '../controller/chat.request.php',
			type: 'POST',
			dataType:'JSON',
			data: {
				requests: 'chatMsg',
				//form: $(this).serialize(),
				user: user,
				msg: msg,
				recipient: rid,
			},
			success: function(e){
				//console.log('e');
				//$("#message").animate({"scrollBottom": $('#message')[0].scrollHeight}, "slow");
				$(".loading").show('slow');
			}, 
		}) }
	});
	
	//get Agent Lists
	var userId = $("#userID").val();
	$.ajax({
		url:'../controller/chat.request.php',
		type:'POST',
		dataType:'JSON',
		data:{
			requests:'getUser',
			agentID: userId,
		},
		success:function(e){
			if(e){
				$("#contact-list").empty();
				for( counter in e) {
					//	console.log(e[counter].full_name);
					$("#contact-list").append('<li id="'+e[counter].id+'" class="userMe"> <span class="badge bg-light-blue chat-counter">1</span><div class="contacts-list-info"><span class="contacts-list-name">'+ e[counter].full_name +'</span></div></li>');
				}
			}
		}
	});
	
	
	//Clone Chat Box
	$(document).on("click", ".userMe", function(){
		var prevUser = $('.chat-user div').text();
		var currID = $("#rid").val();
		var userId = $(this).closest('li').attr('id');
		var otherUser = $("li#"+userId + " .contacts-list-info span").text();
		
		$(".chat-user").empty();
		$(".chat-user").append('<div>'+otherUser+'</div><input type="hidden" id="rid" name="rid" value="'+userId+'">');		
		$("#chat-box").removeClass(".direct-chat-contacts-open");
		
		//step1 -> clear the list from other user
		$(".chat-messages").empty();
		$(".chat-messages").attr('id', userId);
	

	});

	


}); //End Documentation
