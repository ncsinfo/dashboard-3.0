<?php

require_once 'bootstrap/autoload.php';

use App\Libraries\Session;
use App\Libraries\Route;
use App\Controllers\MainController;
use App\Controllers\PaymentController;
use App\Controllers\EmailController;
use App\Helper\Request;
//$newSes = new Session;
//$user = new App\Models\Query;
//$userDets = $user->getUserDetails($_SESSION['user']['whosIN']);

$route = new Route;

$route->add('/', function() {
	//echo 'HOME';
	MainController::index();
});

$route->add('/login', function() {
	//echo 'HOME';
	MainController::login();
});
//Payment
$route->add('/payment', function() {
	//echo 'HOME';
	PaymentController::index();
});

$route->add('/payment/transactions', function() {
	//echo 'HOME';
	PaymentController::transaction();
});

$route->add('/payment/reports', function() {
	//echo 'HOME';
	PaymentController::reports();
});
//Emails
$route->add('/emails/visa', function() {
	//echo 'HOME';
	EmailController::visa();
});
$route->add('/emails/tours', function() {
	//echo 'HOME';
	EmailController::tour();
});
$route->add('/emails/packages', function() {
	//echo 'HOME';
	EmailController::package();
});
$route->add('/emails/others', function() {
	//echo 'HOME';
	EmailController::other();
});

$route->add('/signout', function() {
	//echo 'HOME';
	MainController::logout();
});

//Administration Area
$route->add('/admin/users/create', function() {
	//echo 'HOME';
	EmailController::other();
});
$route->add('/admin/users/edit', function() {
	//echo 'HOME';
	EmailController::other();
});

$route->add('/admin/reports', function() {
	//echo 'HOME';
	EmailController::other();
});

//AJAX REQUEST

$route->add('/ajaxrequest', function() {
	return Request::request();
});

/*
$route->add('/about', function() {
	PageController::view('about');
});

$route->add('/contact', function() {
	PageController::view('contact');
});

$route->add('/hotels', function() {
	HotelController::index();
});

$route->add('/hotels/.+/.+', function($first, $second) {
	HotelController::view($first);
});

//Generic Guide
$route->add('/name', function() {
	echo 'Name Home';
});

$route->add('/name/.+', function($name) {
	echo "Name $name";
});


$route->add('/this/is/the/.+/story/of/.+', function($first, $second) {
	echo "This is the $first story of $second";
});*/


$route->submit();
