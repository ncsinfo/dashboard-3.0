<?php include('view/header.php'); ?>

<?php 

$checkPermission = new App\Models\CheckUserPermission;
//$checkPermission->pagePermision(2,array(1,2,3));
$checkPermission->pagePermision($_SESSION['user']['whosIN'],array(1,2,3));
$thePermission = $checkPermission->thePermission($_SESSION['user']['whosIN']);
//$thePermission = $checkPermission->thePermission(2);
//for search query
$formName = "transactions";
$tempDate = date_create();
$tempDate = date_format($tempDate,"Y-m-d");

$searchFile = '';
if($_GET) {
	$searchFile = $_GET['searchID'];
	//var_dump($_GET);
}
?>

<div class="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item"> <a href="#">Payment</a></li>
          <li class="breadcrumb-item active">Transactions</li>
        </ol>

       <div class="row">
			<!-- Left col -->
			<div class="col-md-12">
				<div class="card ">
					<div class="card-header bg-primary text-light">
						<h3 class="card-title text-center">Client Payment Transactions</h3>
						<form id="sortTable" name="sortTable" class="form-inline pull-right">
							<div class="form-group">
								<label>Sort by date:</label>
								<input id="dateSort1" type="text" name="sortDate" class="form-control" data-provide="datepicker" value="<?php echo $tempDate;?>" readonly="readonly" placeholder="--select date--">
								<input id="searchText" type="hidden" value="<?php echo $searchFile; ?>" >
								<input type="hidden" id="userPermission" value="<?php echo $thePermission;?>" />
							</div>
							<?php if((int)$thePermission == 2) : ?>
								<div class="form-group">
									<label>Sort by agent:</label>
									<select id="sortAgent1" name="sortAgent" class="form-control">
										<option value="">All</option>
										<?php
										$user = new App\Models\Query;
										$agentDetails = $user->getAgents();
										foreach($agentDetails as $value) :?>
											<option value="<?php echo $value['id']; ?>">
												<?php echo $value['full_name']; ?>
											</option>
										<?php endforeach;?>
									</select>
								</div>
							<?php else: ?>
								<input type="hidden" id="sortAgent" name="sortAgent" value="<?php echo $_SESSION['user']['whosIN'] ?>" />
							<?php endif;?>
						</form>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover ">
							<thead>
							<tr>
								<th>Card Holder</th>
								<th>Issuing Bank</th>
								<th>Issuing Country</th>
								<th>Transaction Message</th>
								<th>Receipt No.</th>
								<th>Client Name</th>
								<th>Service Type</th>
								<th>Total Price</th>
								<th>Reference No.</th>
								<th>Client IP Address</th>
								<th>Date</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

<?php include('view/footer.php'); ?>

<script>
    $(function () {
        $('#dateSort1').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
    });
</script>
<script>
	var date = $('#dateSort1').val();
	var searchTxt = $('#searchText').val();
	var agent = $('#agent-ID').val();
	var userPer = $('#userPermission').val();
	var dataTable = $('#example2').DataTable({
		destroy: true,
		draw: true
	});
	tableData(agent,date,searchTxt);
	var dataSet = [];
	$('#dateSort1').on('change',function(){
		if (userPer == 2) {
			agent = $('#sortAgent1').val();
		}
		var date = $(this).val();
		tableData(agent,date,'');
	});
	$('#sortAgent1').on('change', function () {
		var date = $('#dateSort1').val();
		var agent1 = $(this).val();
		tableData(agent1, date, '');
	});
	function dateFormat(dateVal,val) {
		var retDate = '';
		var monthNames = [
			"JAN", "FEB", "MAR",
			"APR", "MAY", "JUN", "JUL",
			"AUG", "SEP", "OCT",
			"NOV", "DEC"
		];

		if (val == 1) {
			dateFormat1(dateVal);
		}else if (val == 2) {
			dateFormat2(dateVal);
		}
		function dateFormat1(dVal) {
			var date = new Date(dVal);
			var day = date.getDate(date);
			var monthIndex = date.getMonth(date) + 1;
			var year = date.getFullYear(date);
			retDate = monthIndex+'/'+day+'/'+year;
			//return monthIndex+'/'+day+'/'+year;
		}
		function dateFormat2(dVal) {
			var date = new Date(dVal);
			var day = date.getDate(date);
			var monthIndex = date.getMonth(date);
			var year = date.getFullYear(date);
			retDate = day+' '+monthNames[monthIndex]+' '+year;
			//return monthNames[monthIndex]+' '+day+', '+year;
		}
		return retDate;
	}
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function tableData(agent,date,searchTxt) {
		console.log(searchTxt);
		$('#preLoader').css('display','block');
		$.ajax({
			url : '/ajaxrequest',
			type:'POST',
			dataType:'JSON',
			data:{
				requests:'dateSortAgentPage',
				dateField : date,
				sortAgent: agent,
				searchTxt: searchTxt
			},
			success:function(e){
				console.log(e);
				dataSet = [];
				for(num in e){
					var nPrice = parseFloat(e[num].price);
					amnt1 = (e[num].currency === 'us' ? '$' : 'AED') + ' ' + nPrice.toFixed(2);
					date1 = dateFormat(e[num].trans_date,2);
					//console.log(e[num].status);
					dataSet.push([
						e[num].onCard_name,
						e[num].card_bank,
						e[num].card_country,
						e[num].status,
						e[num].receiptNo,
						e[num].client_name,
						e[num].trans_title,
						amnt1,
						e[num].ref_num,
						e[num].client_ip,
						date1]
					);
				}
				//console.log(dataSet);
				dataTable.destroy();
				dataTable.draw();
				$('#example2').DataTable({
					data: dataSet,
					columns: [
						{ title: "Client Holder" },
						{ title: "Issuing Bank" },
						{ title: "Issuing Country" },
						{ title: "Transaction Message" },
						{ title: "Receipt No." },
						{ title: "Client Name" },
						{ title: "Service Type" },
						{ title: "Price" },
						{ title: "Reference No." },
						{ title: "Client IP Add." },
						{ title: "Date" }
					]
				});
				dataTable = $('#example2').DataTable();
				$('#preLoader').css('display','none');
			}
		});
	}
	$(document).on('click','td',function(){
		var tokenID = $(this).closest('tr').attr('id');
		console.log(tokenID); //newly added

	});

</script>
