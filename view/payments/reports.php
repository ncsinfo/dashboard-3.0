<?php include('view/header.php'); ?>
<?php
//for search query
$formName = "home";
//MOD: Set date to current
date_default_timezone_set("Asia/Dubai");
$vDate = date_create();
$vDate = date_format($vDate,"Y-m-d");
$transQuery = new App\Libraries\Functions;
$newQuery = $transQuery->agentTransactionDetails($vDate);
$checkPermission = new App\Models\CheckUserPermission;
$checkPermission->pagePermision(2,array(1,2,3));
// $checkPermission->pagePermision($_SESSION['user']['whosIN'],array(1,2,3));
$thePermission = $checkPermission->thePermission(2);
// $thePermission = $checkPermission->thePermission($_SESSION['user']['whosIN']);
?>
<div class="content-wrapper">

	<div class="container-fluid">

		<!-- Breadcrumbs -->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item"> <a href="#">Payment</a></li>
			<li class="breadcrumb-item active">Reports</li>
		</ol>

		<div class="row">
			<div class="col-lg-12">
				<div class="card ">
					<div class="card-header bg-success text-light">
						<h3 class="card-title">Transaction Report List</h3>
						<h4 id="agent-name" class="card-title">(<?php echo strtoupper($userDets['full_name']); ?>)</h4>
						<input type="hidden" id="userPermission" value="<?php echo $thePermission;?>" />
						
					</div>
					<div class="card-body with-border">
						<div class="card-tools pull-left">
							<button type="button" class="form-control btn btn-default pull-left" id="daterange-btn1">
								<span><i class="fa fa-calendar"></i> Select Date Range</span>
								<i class="fa fa-caret-down"></i>
							</button>
							<div>&nbsp;</div>
						</div>
						<?php if((int)$thePermission == 2) : ?>
						<div class="card-tools pull-left">
							<select class="form-control" id="select-agent-id">
								<option value="">--select agent--</option>
								<?php
								$user = new App\Libraries\Functions;
								$agentDetails = $user->getAgents();
								foreach($agentDetails as $value) :?>
								<option value="<?php echo $value['id']; ?>">
									<?php echo $value['full_name']; ?>
								</option>
								<?php endforeach;?>
							</select>
						</div>
						<?php endif;?>
						<div id="gen-btn" class="card-tools pull-left">
							&nbsp;<button type="button" class=" btn btn-primary pull-right" id="report-btn">Generate</button>
							<input type="hidden" id="date1-1" /><br/>
							<input type="hidden" id="date2-1" />
						</div>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover " width="100%">
							<thead>
								<tr>
									<th>Date</th>
									<th>Reference Number</th>
									<th>Client (Payee)</th>
									<th>Transaction Type</th>
									<th>Transaction #</th>
									<th>Receipt #</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<a href="invoice-print.php" target="_blank" id="table-print" class="btn btn-warning"><i class="fa fa-print"></i> Print</a>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- /.container-fluid -->

</div>
<!-- /.content-wrapper -->

<!--Modal-->
<div class="modal fade" id="populateInvoice" tabindex="-1" role="dialog">
	<div id="invoice-data" class="modal-dialog" role="document" style="width:75%;">
		<div class="modal-content">
			<div class="modal-header">
				<input type="hidden" id="transID" />
				<button type="button" id="btn-close-0" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<section class="content-header">
					<h1>Invoice &nbsp;<small id="invoice-num"></small></h1>
				</section>
			</div>
			<div class="modal-body popInput" id="populateBody">
				<div id="print-note" class="pad margin no-print">
					<div class="callout callout-info" style="margin-bottom: 0!important;">
						<h4><i class="fa fa-info"></i> Note:</h4>
						This page has been enhanced for printing. Click the print button at the bottom of this Invoice.
					</div>
				</div>
				<section class="invoice">
					<!-- title row -->
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								<i class="fa fa-globe"></i> South Travels & Tourism
								<small class="pull-right">Date: <?php echo date('m/d/Y');?></small>
							</h2>
						</div>
						<!-- /.col -->
					</div>
					<!-- info row -->
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							From
							<address>
								<b><?php echo strtoupper($userDets['full_name']); ?></b><br>
								<?php echo $userDets['email']; ?>
							</address>
						</div>
						<!-- /.col -->
						<div class="col-sm-4 invoice-col">
							To
							<address>
								<b><span id="client-name"></span></b><br>
								<span id="client-email"></span>
							</address>
						</div>
						<!-- /.col -->
						<div class="col-sm-4 invoice-col">
							<b>Invoice: <span id="invoice-num-0"></span></b><br>
							<b>Reference #:</b><span id="ref-num"></span><br>
							<b>Issuing Bank:</b><span id="card-bank"></span><br>
							<b>Payment Due:</b><span id="payment-due"></span><br><br>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->

					<!-- Table row -->
					<div class="row">
						<div class="col-xs-12 table-responsive">
							<table class="table table-striped" id="trans-table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Transaction #</th>
										<th>Receipt #</th>
										<th>Description</th>
										<th>Subtotal</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
					<hr/>
					<div class="row">
						<!-- accepted payments column -->
						<div class="col-xs-6">
							<p class="lead">Payment Methods:</p>
							<img src="dist/img/credit/visa.png" alt="Visa">
							<img src="dist/img/credit/mastercard.png" alt="Mastercard">
							<img src="dist/img/credit/masterpass.png" alt="American Express">
						</div>
						<!-- /.col -->
						<div class="col-xs-4 pull-right">
							<p class="lead">Amount Due</p>

							<div class="table-responsive">
								<table class="table pull-right">
									<tr>
										<th style="width:50%">Subtotal:</th>
										<td id="subTotal" class="pull-right">0.00</td>
									</tr>
									<tr>
										<th>Fees:</th>
										<td id="fees" class="pull-right">0.00</td>
									</tr>
									<tr>
										<th>Discounts:</th>
										<td id="discounts" class="pull-right">(0.00)</td>
									</tr>
									<tr>
										<th>Total:</th>
										<td id="total" class="pull-right"><strong>0.00</strong></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->

					<!-- this row will not appear when printing -->
					<div class="row no-print">
						<div class="col-xs-12">
							<a href="invoice-print.php" target="_blank" id="invoice-print" class="btn btn-warning"><i class="fa fa-print"></i> Print</a>
							<button type="button" id="btn-close" class="btn btn-primary pull-right" data-dismiss="modal"><i class="fa fa-close"></i> Close
							</button>
							<button type="button" id="gen-pdf" class="btn btn-success pull-right" style="margin-right: 5px;">
								<i class="fa fa-download"></i> Generate PDF
							</button>
						</div>
					</div>
				</section>
				<div class="clearfix"></div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<?php include('view/footer.php'); ?>

<!--//MOD: Add AJAX for date change function -->
<script>

	var retrieveRec=[];
	var txtDate1 = $('#date1').val();
	var txtDate2 = $('#date2').val();
	var txtDate11 = $('#date1-1').val();
	var txtDate21 = $('#date2-1').val();
	var chart = $("#barChart");
	var agentID = $("#agent-ID").val();
	var agentTable = $('#example2 tbody');
	var dataTable = $('#example2').DataTable( {
		destroy: true,
		draw: true,
	});
	$('#daterange-btn').daterangepicker(
		{
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 15 Days': [moment().subtract(15, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			startDate: moment().subtract(29, 'days'),
			endDate: moment()

		},
		function (start, end) {
			$('#daterange-btn span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
			$('#date1').val(start.format('YYYY-MM-DD'));
			$('#date2').val(end.format('YYYY-MM-DD'));
			txtDate1 = $('#date1').val();
			txtDate2 = $('#date2').val();
			chart.css('width','400px');
		}
	);
	$('#daterange-btn1').daterangepicker(
		{
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 15 Days': [moment().subtract(15, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			startDate: moment().subtract(29, 'days'),
			endDate: moment()

		},
		function (start, end) {
			$('#daterange-btn1 span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
			$('#date1-1').val(start.format('YYYY-MM-DD'));
			$('#date2-1').val(end.format('YYYY-MM-DD'));
			txtDate11 = $('#date1-1').val();
			txtDate21 = $('#date2-1').val();
			chart.css('width','400px');
		}
	);
	$('#report-btn').on('click',function(){
		event.preventDefault();
		var userPer = $('#userPermission').val();
		if (userPer == 2) {
			agentID = $('#select-agent-id').val();
		}
		console.log(userPer);
		dataTable.destroy();
		dataTable.draw();
		$('#example2').DataTable({
			ajax:{
				url : '/ajaxrequest',
				type:'POST',
				dataType:'JSON',
				dataSrc:"",
				data:{
					requests:'agentTransReport',
					dateFrom: txtDate11,
					dateTo: txtDate21,
					agentID: agentID
				}
			},
			columns:[
				{ data: "trans_date" },
				{ data: "refNun" },
				{ data: "client_name" },
				{ data: "trans_title" },
				{ data: "transNum" },
				{ data: "receiptNo" },
				{ data: "amount"}
			],
			createdRow:function(row,data,dataIndex){
				retrieveRec.push(data);
				$(row).attr('id',data.id);
			}
		});
		dataTable = $('#example2').DataTable();
	});
	var table = $('#trans-table tbody');
	$('#example2').on( 'click', 'tr', function () {
		var id = $(this).closest('tr').attr('id');
		console.log(id);
		if(id != undefined){
			$('#populateInvoice').modal();
			$('#transID').val(id);
			$.ajax({
				url : '/ajaxrequest',
				type:'POST',
				dataType:'JSON',
				data:{
					requests:'invoiceData',
					transID: id
				},
				success:function(e){
					console.log(e);
					var currency = 'AED';
					var subTotal = 0;
					var fees = 0;
					var discount =0;
					var totalDue =0;
					table.empty();
					for(num in e){
						$('#card-bank').text(e[num].card_bank.toUpperCase());
						$('#invoice-num').text('#' + e[num].transNum);
						$('#invoice-num-0').text('#' + e[num].transNum);
						$('#client-name').text(e[num].client_name.toUpperCase());
						$('#client-email').text(e[num].client_email);
						$('#ref-num').text(e[num].refNun);
						table.append('<tr><td>'+e[num].trans_date+'</td><td>'+e[num].transNum+'</td><td>'+e[num].receiptNo+'</td><td>'+e[num].trans_title.toUpperCase()+'</td><td class="pull-right">'+e[num].amount+'</td></tr>');
						subTotal = subTotal + parseFloat(e[num].price);
						if(e[num].currency =='us'){
							currency = '$';
						}
					}
					totalDue = (subTotal + fees) - discount;
					$('#subTotal').text(currency + ' ' + subTotal.toFixed(2));
					$('#total').text(currency + ' ' + totalDue.toFixed(2));
					$('#payment-due').text(currency + ' ' + totalDue.toFixed(2));
				}

			});
		}

	} );
	$('#invoice-print').on('click',function() {
		event.preventDefault();
		$('#invoice-data').css('width','97%');
		$('#invoice-print').css('opacity','0');
		$('#btn-close-0').css('opacity','0');
		$('#btn-close').css('opacity','0');
		$('#gen-pdf').css('opacity','0');
		$('#print-note').css('display','none');
		$('#populateInvoice').css('font-size','0.4em');
		var divToPrint=$('#populateInvoice').html();
		var newWin=window.open('','Print-Window');
		newWin.document.open();
		newWin.document.write('<html><style>@media print{@page {size: portrait}}</style><link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css"><link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> <link rel="stylesheet" type="text/css" href="css/style.css"><body onload="window.print()">'+divToPrint+'</body></html>');
		newWin.document.close();
		setTimeout(function(){newWin.close();},1);
		$('#populateInvoice').css('font-size','1em');
		$('#invoice-data').css('width','75%');
		$('#invoice-print').css('opacity','1');
		$('#btn-close-0').css('opacity','1');
		$('#btn-close').css('opacity','1');
		$('#gen-pdf').css('opacity','1');
		$('#print-note').css('display','block');

	});
	$('#table-print').on('click',function() {
		event.preventDefault();

		$('#example2').css('font-size','0.85em');
		$('#gen-btn').css('opacity','0');
		$('#table-print').css('opacity','0');
		$('#agent-name').css('display','block');
		var divToPrint=$('#table-content').html();
		var style = '<style> @media print{@page {size: landscape}}#example2 td:nth-child(2),#example2 td:nth-child(5),#example2 td:nth-child(6),#example2 th {text-align : center;}#example2 td:nth-child(7){text-align : right;}#example2 td:nth-child(4){width:23%;}#example2 td:nth-child(3){width:20%;}span {font-weight:normal;}</style>';
		var newWin=window.open('','Print-Window');
		newWin.document.open();
		newWin.document.write('<html><link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css"><link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> <link rel="stylesheet" type="text/css" href="css/style.css">'+style+'<body onload="window.print()">'+divToPrint+'</body></html>');
		newWin.document.close();
		setTimeout(function(){newWin.close();},10);
		$('#agent-name').css('display','none');
		$('#example2').css('font-size','1em');
		$('#gen-btn').css('opacity','1');
		$('#table-print').css('opacity','1');


	});
</script>
