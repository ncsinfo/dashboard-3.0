<?php include('view/header.php'); ?>

<?php 

date_default_timezone_set("Asia/Dubai");
      $vDate = date_create();
      $vDate = date_format($vDate,"Y-m-d");
      $name =  $userDets['full_name'];
      $pre = substr($name, 0,3);
      $day = date('YmdHis');
      $successTrans = $user->transStatusSuccess($_SESSION['user']['whosIN']);
      $failedTrans = $user->transStatusFailed($_SESSION['user']['whosIN']);

?>

<div class="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item"> <a href="#">Payment</a></li>
          <li class="breadcrumb-item active">Generate</li>
        </ol>

        <div class="row">
        <!-- Left col -->
        <div class="col-md-4">
			<div class="card ">
				<div class="card-header bg-primary text-light">
					<h3 class="card-title text-center">Generate Payment Link</h3>
				</div>
				<div class="card-body">
					<form id="genToken">
        				<div class="form-group">
        					<select class="form-control" name="currency" id="currency">
        						<option value="us">($) USD </option>
        						<option value="aed">(AED) Dirham</option>
        					</select>
        				</div>
        				<div class="form-group">
        					<input type="text" id="payAmount" name="figure" class="form-control" placeholder="Total amount to be paid">
        				</div>
        				<div class="form-group">
        					<input type="text" id="payFor" name="payfor" class="form-control" placeholder="Payment for ? " maxlength="34">
        				</div>
        				<div class="form-group">
        					<input type="text" id="refCode" name="refCode" class="form-control" placeholder="Enter Reference Number" value="<?php echo strtoupper($pre) . "-" . $day;?>">
                  <input type="hidden" name="refName" id="refName" class="form-control" placeholder="Enter Reference Number" value="<?php echo strtoupper($pre);?>">
                        </div>
        				<div class="form-group">
        					<input type="text" id="payee" name="fullname" class="form-control" placeholder="Client Full name">
        				</div>
        				<div class="form-group">
        					<input type="text" id="client-email" name="email" class="form-control" placeholder="Client Email Address">
        				</div>
        				<button class="btn btn-primary btn-block">Generate</button>
        			</form>
				</div>
			</div>
        	
        </div>
         <div class="col-md-8">
        	<div class="card">
                <div class="card-header bg-primary with-border">
                    <h3 class="card-title text-center">Generated Payment Link Status</h3>
                    <!--MOD: Add Sorting date option -->
                </div>
                <div class="card-body">
                    <form class="pull-left">
                        <div class="input-group pull-left">
                            <span class="pull-left">Sort by Date: &nbsp;</span><input style="width: 150px;" class="form-control" type="date" id="date-Sort" name="viewDate" value="<?php echo $vDate; ?>"/>
                        </div>
                    </form>
                    <form id="search-frm" style="width: 200px;" class="pull-right">
                        <div class="input-group pull-right" style="border:1px solid rgba(0,0,0,0.15);">
                            <input id="search-text" style="outline:none;border:none;" type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
        		<div class="card-body">
					<div class="table-responsive">
					<!--	<table class="dataTables_wrapper container-fluid dt-bootstrap4" id="PaymentLinksStatus">-->
						<table class="dataTables_wrapper container-fluid dt-bootstrap4" id="exampl">
        				<thead>
        					<th>Client Name</th>
        					<th>Payment For</th>
        					<th>Token</th>
        					<th>Amount</th>
        					<th>Attempts</th>
        					<th>Status</th>
        					<th>Date Created</th>
        				</thead>
        				<tbody>
        					<?php 
        						$query = new App\Models\Query;
                                //MOD: change function to function(transactions with date sorting)
                                if ($_GET) {
                                    $search = $_GET['q'];
                                    $transactions = $query->getTransactions_search($_SESSION['user']['whosIN'],$search);
                                  //  $transactions = $query->getTransactions_search(2,$search);
                                }
                                else {
                                    $transactions = $query->getTransactions_wDate($_SESSION['user']['whosIN'], $vDate);
                                  //  $transactions = $query->getTransactions_wDate(2, $vDate);
                                }
        						foreach($transactions as $trans) : 
        					?>
		    					<tr id="<?php echo $trans['tokenID'];?>">
		    						<td id="pop"><?php echo $trans['client_name'];?></td>
		    						<td><?php echo $trans['trans_title'];?></td>
		    						<td  id="token" class="<?php echo $trans['token'];?>"><?php echo $trans['token'];?></td>
		    						<td><?php echo $trans['price'];?></td>
		    						<td><?php echo $trans['attempts'];?></td>
		    						<td><?php echo $trans['status'];?></td>
                                    <!-- Format Date-->
                                    <td><?php $cDate = date_create($trans['date_create']); echo date_format($cDate,'M j Y');?></td>
                    <td><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></td>
		    					</tr>
        					<?php endforeach; ?>
        				</tbody>
        			</table>
					</div>
        			
        		</div>
        	</div>
			 <div id="generatedlink"></div>
        </div>
     </div>

      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->


<!-- Modals -->
 <!-- /.content-wrapper -->
 <div class="modal fade" id="paymentLinkModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">New Payment Link Created</h4>
      </div>
      <div class="modal-body">
          <input type="text" id="holdText-0" style="outline: none; width: 100%;">
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" id="btn-copy-0">Send Link</button>
        <button type="button" class="btn btn-default" id="btn-close-modal" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- /.content-wrapper for previously generated links-->
<div class="modal fade" id="paymentLinkGen" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Payment Link</h4>
            </div>
            <div class="modal-body" id="tokenLink-body">
                <input type="text" id="holdText" style="outline: none; width: 100%;">
                <input type="hidden" id="tokenText" style="outline: none; width: 100%;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" id="btn-copy">Send Link</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="populateClient" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Client Data</h4>
            </div>
            <div class="modal-body popInput" id="populateBody">
                <label for="popClientName"><i>Client Name:</i></label>
                <input type="text" id="popClientName" /><br/>
                <label for="popClientEmail"><i>Client Email:</i></label>
                <input type="text" id="popClientEmail" /><br/>
                <label for="popClientTrans"><i>Transaction Type:</i></label>
                <input type="text" id="popClientTrans" /><br/>
                <label for="popClientAmount"><i>Amount Paid:</i>&nbsp;<span id="amount-paid"></span></label>
                <input type="hidden" id="popClientAmount" />
                <input type="hidden" id="popClientAmountCur" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal" id="popLink">Populate</button>
                <!--<button type="button" id="sendLink" class="btn btn-primary" onclick = "parent.location='mailto:abc@abc.com'">Send Link</button>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="emailBody" style="opacity: 0;"></div>
<?php include('view/footer.php'); ?>
<script>

</script>
<!-- Page Scripts -->
<!--//MOD: Add AJAX for date change function -->
<script>
    /*Generate Reference Number After clicking Modal Close*/
    $('#btn-close-modal').on('click',function() {
        var aName = $('#refName').val();
        $.ajax({
            url : '/ajaxrequest',
            type:'POST',
            data:{
                requests:'createReferenceNum',
                name: aName
            },
            success:function(e){
                $('#refCode').val(e);
            }
        })
    });
    // Format Date
    function dateFormat(dateVal) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];
        var date = new Date(dateVal);
        var day = date.getDate(date);
        var monthIndex = date.getMonth(date);
        var year = date.getFullYear(date);
        return monthNames[monthIndex]+' '+day+' '+year;
    }
    $('#search-frm').on('submit',function(){
        event.preventDefault();
        var searchText = $('#search-text').val();
        var agentID = $('#agent-ID').val();
        console.log(searchText);
        $('#preLoader').css('display','block');
        $.ajax({
            url : '/ajaxrequest',
            type:'POST',
            dataType:'JSON',
            data:{
                requests:'searchAgentPaymentLink',
                searchTxt : searchText,
                sortAgent: agentID
            },
            success:function(e){
                console.log(e);
                if(e){
                    tableClientBody.empty();
                    for(counter in e){
                        console.log(e);
                        tableClientBody.append('<tr id='+e[counter].tokenID+'><td id="pop">'+e[counter].client_name+'</td><td>'+e[counter].trans_title+'</td><td id="token" class="'+e[counter].token+'">'+e[counter].token+'</td><td>'+e[counter].price+'</td><td>'+e[counter].attempts+'</td><td>'+e[counter].status+'</td><td>'+dateFormat(e[counter].date_create)+'</td><td><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></td>');
                    }
                    $('#preLoader').css('display','none');
                }
            }
        })

    });

</script>
