<footer class="sticky-footer">
	<div class="container">
		<div class="text-center">
			<small>Copyright &copy; Your Website 2017</small>
		</div>
	</div>
</footer>

<!-- Scroll to Top Button -->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fa fa-angle-up"></i>
</a>

<!-- Logout Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Select "Logout" below if you are ready to end your current session.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<a class="btn btn-primary" href="login.html">Logout</a>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="/public/sb-admin/vendor/jquery/jquery.min.js"></script>
<script src="/public/sb-admin/vendor/popper/popper.min.js"></script>
<script src="/public/sb-admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/public/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
<!--<script src="/public/sb-admin/vendor/chart.js/Chart.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>-->
<script src="/public/sb-admin/vendor/datatables/jquery.dataTables.js"></script>
<script src="/public/sb-admin/vendor/datatables/dataTables.bootstrap4.js"></script>
<script src="/public/js/jquery.validate.min.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/public/sb-admin/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Custom scripts for this template -->
<script src="/public/sb-admin/js/sb-admin.js"></script>
<script src="/public/js/request_ui.js"></script>

</body>

</html>