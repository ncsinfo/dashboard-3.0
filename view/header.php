<?php 

$newSes = new App\Libraries\Session;
$user = new App\Models\Query;
//$userDets = $user->getUserDetails(2);
$userDets = $user->getUserDetails($_SESSION['user']['whosIN']);
?>
<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<meta name="description" content="">
		<meta name="author" content="">
		<title>SB Admin - Start Bootstrap Template</title>

		<!-- Bootstrap core CSS -->
		<link href="/public/sb-admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom fonts for this template -->
		<link href="/public/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Plugin CSS -->
		<link href="/public/sb-admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="/public/sb-admin/css/sb-admin.css" rel="stylesheet">
		<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">

	</head>

	<body class="fixed-nav sticky-footer bg-dark" id="page-top">

		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="#">Start Bootstrap</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item active" data-toggle="tooltip" data-placement="right">
						<a class="nav-link" href="/">
							<i class="fa fa-fw fa-dashboard"></i>
							<span class="nav-link-text">
								Dashboard</span>
						</a>
					</li>
					
					<li class="nav-item" data-toggle="tooltip" data-placement="right">
						<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
							<i class="fa fa-fw fa-wrench"></i>
							<span class="nav-link-text">
								Payment</span>
						</a>
						<ul class="sidenav-second-level collapse" id="collapseComponents">
							<li><a href="/payment">Generate Payment Link</a></li>
							<li><a href="/payment/transactions">View Payment Transactions</a></li>
							<li><a href="/payment/reports">View Transaction Reports</a></li>
						</ul>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right">
						<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#emailComponents" data-parent="#exampleAccordion">
							<i class="fa fa-fw fa-envelope"></i>
							<span class="nav-link-text">
								Emails</span>
						</a>
						<ul class="sidenav-second-level collapse" id="emailComponents">
							<li><a href="/emails/visa">Visa Applications</a></li>
							<li><a href="/emails/tours">Tours Applications</a></li>
							<li><a href="/emails/packages">Holiday Packages</a></li>
							<li><a href="/emails/others">Others</a></li>
						</ul>
					</li>

					<li class="nav-item" data-toggle="tooltip" data-placement="right">
						<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
							<i class="fa fa-fw fa-sitemap"></i>
							<span class="nav-link-text">
								Administrator</span>
						</a>
						<ul class="sidenav-second-level collapse" id="collapseMulti">
							<li>
								<a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti1">Users</a>
								<ul class="sidenav-third-level collapse" id="collapseMulti1">
									<li>
										<a href="#">Add User</a>
									</li>
									<li>
										<a href="#">Edit User</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Reports</a>
								<ul class="sidenav-third-level collapse" id="collapseMulti2">
									<li>
										<a href="#">User Sales Reports</a>
									</li>
									<li>
										<a href="#">Email Reports</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right">
						<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#helpLink" data-parent="#exampleAccordion">
							<i class="fa fa-fw fa-wrench"></i>
							<span class="nav-link-text">
								Help</span>
						</a>
						<ul class="sidenav-second-level collapse" id="helpLink">
							<li><a href="static-nav.html">General</a></li>
							<li><a href="#">Visa(Agent)</a></li>
							<li><a href="#">Calendar</a></li>
							<li><a href="#">Report</a></li>
						</ul>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right">
						<a class="nav-link" href="/signout">
							<i class="fa fa-fw fa-link"></i>
							<span class="nav-link-text">
								Sign Out</span>
						</a>
					</li>
				</ul>
				<ul class="navbar-nav sidenav-toggler">
					<li class="nav-item">
						<a class="nav-link text-center" id="sidenavToggler">
							<i class="fa fa-fw fa-angle-left"></i>
						</a>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle mr-lg-2" href="#" id="messagesDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-fw fa-envelope"></i>
							<span class="d-lg-none">Messages
								<span class="badge badge-pill badge-primary">12 New</span>
							</span>
							<span class="new-indicator text-primary d-none d-lg-block">
								<i class="fa fa-fw fa-circle"></i>
								<span class="number">12</span>
							</span>
						</a>
						<div class="dropdown-menu" aria-labelledby="messagesDropdown">
							<h6 class="dropdown-header">New Messages:</h6>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<strong>David Miller</strong>
								<span class="small float-right text-muted">11:21 AM</span>
								<div class="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<strong>Jane Smith</strong>
								<span class="small float-right text-muted">11:21 AM</span>
								<div class="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<strong>John Doe</strong>
								<span class="small float-right text-muted">11:21 AM</span>
								<div class="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item small" href="#">
								View all messages
							</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle mr-lg-2" href="#" id="alertsDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-fw fa-bell"></i>
							<span class="d-lg-none">Alerts
								<span class="badge badge-pill badge-warning">6 New</span>
							</span>
							<span class="new-indicator text-warning d-none d-lg-block">
								<i class="fa fa-fw fa-circle"></i>
								<span class="number">6</span>
							</span>
						</a>
						<div class="dropdown-menu" aria-labelledby="alertsDropdown">
							<h6 class="dropdown-header">New Alerts:</h6>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<span class="text-success">
									<strong>
										<i class="fa fa-long-arrow-up"></i>
										Status Update</strong>
								</span>
								<span class="small float-right text-muted">11:21 AM</span>
								<div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<span class="text-danger">
									<strong>
										<i class="fa fa-long-arrow-down"></i>
										Status Update</strong>
								</span>
								<span class="small float-right text-muted">11:21 AM</span>
								<div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">
								<span class="text-success">
									<strong>
										<i class="fa fa-long-arrow-up"></i>
										Status Update</strong>
								</span>
								<span class="small float-right text-muted">11:21 AM</span>
								<div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item small" href="#">
								View all alerts
							</a>
						</div>
					</li>
					<li class="nav-item">
						<form class="form-inline my-2 my-lg-0 mr-lg-2">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search for...">
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/signout">
							<i class="fa fa-fw fa-sign-out"></i>
							Logout</a>
					</li>
				</ul>
			</div>
    </nav>