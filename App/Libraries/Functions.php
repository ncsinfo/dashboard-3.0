<?php
namespace App\Libraries;

use App\Libraries\Database;
use \PDO;
use \DateInterval;
use \DateTime;
use \DatePeriod;
class Functions extends Database {
	
	public function getDate($start, $end, $format = 'Y-m-d')
	{
	$array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
	}
	
	public function getClientTransactionAttDate($agentid,$date,$searchID){
		
		$date=date_create($date);
		$date =  date_format($date,"Y-m-d");
		
        if(!empty($searchID)) {
            $query = parent::prepare("SELECT tb1.id,tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.id =:Sid");
            $query->execute(array(':Sid' => $searchID));
        }
        else {
            if(!empty($agentid)) {
                $query = parent::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id AND tb1.trans_date = :todayD ORDER BY tb1.id DESC");
                $query->execute(array(':id' => $agentid,
                    ':todayD' => $date));
            }
            else
            {
                $query = parent::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.trans_date = :todayD  ORDER BY tb1.id DESC");
                $query->execute(array(':todayD'=>$date));
            }
        }
		return $query->fetchAll(PDO::FETCH_ASSOC);
		
	}
	public function getClientTransactionAttAgent($agentid){
		$query = parent::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id AND tb1.trans_date = :todayD  ORDER BY tb1.id DESC");
		$query->execute(array(':id'=>$agentid,
							  ':agent'=>'2017-01-30'));
		return $query->fetchAll();
	}
	public function getAgents(){
		$query = parent::prepare("SELECT * from vpcpay_users WHERE permission = 1");
		$query->execute();
		return $query->fetchAll();
	}
    public function getSpecificAgents($agentId){
        $query = parent::prepare("SELECT * from vpcpay_users WHERE id=:agentID");
        $query->execute(array(':agentID'=>$agentId));
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function notifyTransactions_view($agentid){
        $query = parent::prepare("SELECT tb1.id,CONCAT(CASE WHEN tb2.currency = 'us' THEN '$' ELSE 'AED' END,' ',FORMAT(tb2.price,2)) AS amount,tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 JOIN vpcpay_url_token tb0 ON tb1.token = tb0.token JOIN vpcpay_payment_trans as tb2 ON tb0.id = tb2.tokenID WHERE tb1.agent_id =:id AND tb1.notify_stat > :stat ORDER BY tb1.trans_date DESC");
        $query->execute(array(':id' => $agentid,
            ':stat' =>0));
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
	//Modification (view by Date for payment link)
	public function getTransactions_wDate($agentid,$wDate){
		
		$date=date_create($wDate);
		$wDate =  date_format($date,"Y-m-d");
		
		$query = parent::prepare("SELECT tb1.client_name,tb1.status,tb1.date_create,tb1.price,tb1.trans_title,tb1.tokenID,tb2.token,tb2.attempts from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.agent_id = :id AND tb1.date_create = :fDate ORDER BY tb1.id DESC");
		$query->execute(array(':id'=>$agentid,
			                  ':fDate'=>$wDate));
		$result=$query->fetchAll();
		return $result;
	}
    //Modification (view by search name for payment link)
    public function getTransactions_search($agentid,$search){
        $query = parent::prepare("SELECT tb1.client_name,tb1.status,tb1.date_create,tb1.price,tb1.trans_title,tb1.tokenID,tb2.token,tb2.attempts from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.agent_id = :id AND tb1.client_name like :cName ORDER BY tb1.id DESC");
        $query->execute(array(':id'=>$agentid,
            ':cName'=>'%'.$search.'%'));
        $result=$query->fetchAll();
        return $result;
    }
    //Modification (view by search name for payment link)
    public function getClientTransactionAttSearch($agentid,$search){
        $query = parent::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id AND tb1.onCard_name like :cardName ORDER BY tb1.id DESC");
        $query->execute(array(':id'=>$agentid, ':cardName'=>'%'.$search.'%'));
        return $query->fetchAll();
    }
    //Modification (transaction status SUCCESSFUL count)
    public function transStatusSuccess($agentId){
        $stat = "Transaction Successful";
        $query = parent::prepare("SELECT count(*) as total from vpcpay_trans_att WHERE status = :stat AND agent_id = :agentID");
        $query->execute(array(':agentID'=>$agentId,':stat'=>$stat));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //Modification (transaction status FAILED count)
    public function transStatusFailed($agentId){
        $stat = "Transaction Successful";
        $query = parent::prepare("SELECT count(*) as total from vpcpay_trans_att WHERE status <> :stat AND agent_id = :agentID");
        $query->execute(array(':agentID'=>$agentId,':stat'=>$stat));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //Modification (view by search name for payment link)
    public function clientPaymentLinkForEmail($clientId){
        $query = parent::prepare("SELECT tb1.refNun,tb1.trans_title,tb1.client_name,tb1.price,tb1.client_email,tb1.currency,tb2.token from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.tokenID =:id");
        $query->execute(array(':id'=>$clientId));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //Modification (view by search name for payment link)
    public function populateClient($clientId){
        $query = parent::prepare("SELECT trans_title,client_name,price,client_email,currency from vpcpay_payment_trans  WHERE tokenID =:id");
        $query->execute(array(':id'=>$clientId));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //For Reports functions
    public function generateAgentReport($agentId,$dateFrom,$dateTo){
        $query0 = parent::prepare("SELECT count(id) from vpcpay_trans_att  WHERE agent_id =:id AND status =:status AND trans_date >=:date1 AND trans_date <=:date2");
        $query1 = parent::prepare("SELECT tb1.price,tb1.currency,tb2.trans_date from vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1 ON tb0.id = tb1.tokenID WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr ORDER BY tb2.trans_date");
        $query2 = parent::prepare("SELECT tb1.price,tb1.currency,tb2.trans_date from vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1 ON tb0.id = tb1.tokenID WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr ORDER BY tb2.trans_date");
        $query3 = parent::prepare("SELECT full_name from vpcpay_users  WHERE id =:id");
        $query0->execute(array(':id'=>$agentId,
            ':status'=>'Transaction Successful',
            ':date1'=>$dateFrom,
            ':date2'=>$dateTo));
        $query1->execute(array(':id'=>$agentId,
            ':status'=>'Transaction Successful',
            ':date1'=>$dateFrom,
            ':date2'=>$dateTo,
            ':curr'=>'aed'));
        $query2->execute(array(':id'=>$agentId,
            ':status'=>'Transaction Successful',
            ':date1'=>$dateFrom,
            ':date2'=>$dateTo,
            ':curr'=>'us'));
        $query3->execute(array(':id'=>$agentId));
        $result0=$query0->fetch(PDO::FETCH_ASSOC);
        $result1=$query1->fetchAll(PDO::FETCH_ASSOC);
        $result2=$query2->fetchAll(PDO::FETCH_ASSOC);
        $result3=$query3->fetch(PDO::FETCH_ASSOC);
        $var = array();
        $var['CNT'] = $result0;
        $var['AED'] = $result1;
        $var['USD'] = $result2;
        $var['AGT'] = $result3;
//        $var = array();
//        array_push($var,$result1);
//        array_push($var,$result2);
        array_merge($var['AED'],$var['USD'],$var['CNT'],$var['AGT']);
        return($var);
    }
    public function agentDataReport($dateFrom,$dateTo){
        $agentData = '';
        $query = parent::prepare("SELECT id,full_name from vpcpay_users  WHERE permission =:per");
        $query->execute(array(':per'=>'1'));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $key => $val) {
            $aID = '';
            $amntAED = 0;
            $amntUSD = 0;
            $noClient = 0;
            $name = '';
            foreach($val as $k  => $v){
                if($k == 'id') {
                    $query0 = parent::prepare("SELECT count(id) from vpcpay_trans_att  WHERE agent_id =:id AND status =:status AND trans_date >=:date1 AND trans_date <=:date2");
                    $query0->execute(array(':id'=>$v,
                        ':status'=>'Transaction Successful',
                        ':date1'=>$dateFrom,
                        ':date2'=>$dateTo));
                    $query1 = parent::prepare("SELECT sum(tb1.price) as totalAmount0 from vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1 ON tb0.id = tb1.tokenID WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr");
                    $query1->execute(array(':id'=>$v,
                        ':status'=>'Transaction Successful',
                        ':date1'=>$dateFrom,
                        ':date2'=>$dateTo,
                        ':curr'=>'aed'));
                    $query2 = parent::prepare("SELECT sum(tb1.price) as totalAmount1 from vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1 ON tb0.id = tb1.tokenID WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr");
                    $query2->execute(array(':id'=>$v,
                        ':status'=>'Transaction Successful',
                        ':date1'=>$dateFrom,
                        ':date2'=>$dateTo,
                        ':curr'=>'us'));
                    $row1 = $query1->fetch(PDO::FETCH_ASSOC);
                    $row2 = $query2->fetch(PDO::FETCH_ASSOC);
                    $row3=$query0->fetchColumn();
                    $amntAED = $row1['totalAmount0'];
                    $amntUSD = $row2['totalAmount1'];
                    $noClient = $row3;
                    $aID = $v;
                }
                if($k == 'full_name') {
                    $name = $v;
                }
            }
            $agentData[$aID]['client'] = $noClient;
            $agentData[$aID]['aed'] = $amntAED;
            $agentData[$aID]['usd'] = $amntUSD;
            $agentData[$aID]['name'] = $name;
        }
        return($agentData);
    }
  //For Reports functions
    public function agentTransactionReport($agentId,$dateFrom,$dateTo){

        if($agentId <> "") {
            $query = parent::prepare("SELECT tb2.id,tb1.refNun,tb1.trans_title,CONCAT(CASE WHEN tb1.currency = 'us' THEN '$' ELSE 'AED' END,' ',FORMAT(tb1.price,2)) AS amount,tb1.client_name,tb2.trans_date,tb2.receiptNo,tb2.transNum from  vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1 ON tb0.id = tb1.tokenID  WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 ORDER BY tb2.trans_date");
            $query->execute(array(':id'=>$agentId,
                ':status'=>'Transaction Successful',
                ':date1'=>$dateFrom,
                ':date2'=>$dateTo));
        }
        else {
            $query = parent::prepare("SELECT tb2.id,tb1.refNun,tb1.trans_title,CONCAT(CASE WHEN tb1.currency = 'us' THEN '$' ELSE 'AED' END,' ',FORMAT(tb1.price,2)) AS amount,tb1.client_name,tb2.trans_date,tb2.receiptNo,tb2.transNum from  vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1 ON tb0.id = tb1.tokenID  WHERE tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 ORDER BY tb2.trans_date");
            $query->execute(array(':status'=>'Transaction Successful',
                ':date1'=>$dateFrom,
                ':date2'=>$dateTo));
        }
        //$query = parent::prepare("SELECT tb2.id,tb1.refNun,tb1.trans_title,CONCAT(CASE WHEN tb1.currency = 'us' THEN '$' ELSE 'AED' END,' ',FORMAT(tb1.price,2)) AS amount,tb1.client_name,tb2.trans_date,tb2.receiptNo,tb2.transNum from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 ORDER BY tb2.trans_date");

        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return($result);
    }
    public function agentTransactionDetails($transId){
        $query = parent::prepare("SELECT distinct tb2.card_bank,tb2.id,tb1.refNun,tb1.trans_title,CONCAT(CASE WHEN tb1.currency = 'us' THEN '$' ELSE 'AED' END,' ',FORMAT(tb1.price,2)) AS amount,tb1.price,tb1.currency,tb1.client_name,tb1.client_email,tb2.trans_date,tb2.receiptNo,tb2.transNum from vpcpay_trans_att tb2 JOIN vpcpay_url_token tb0 ON tb2.token = tb0.token JOIN vpcpay_payment_trans tb1  ON tb0.id = tb1.tokenID  WHERE tb2.id =:id ORDER BY tb2.trans_date");
        $query->execute(array(':id'=>$transId));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return($result);
    }
    //For Reports functions
    //Modification (view by Date for payment link)
    public function viewTransactions_Dashboard($agentid,$fromDate,$toDate){
        $query0 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND date_create > :fDate AND date_create <= :tDate");
        $query0->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate));
        $result0=$query0->fetchColumn();
        $query1 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND status = :stat AND date_create > :fDate AND date_create <= :tDate");
        $query1->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>'Paid'));
        $result1=$query1->fetchColumn();
        $query2 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND status = :stat AND date_create > :fDate AND date_create <= :tDate");
        $query2->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>'Pending'));
        $result2=$query2->fetchColumn();
        $query3 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND status = :stat AND date_create >= :fDate AND date_create <= :tDate");
        $query3->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>'Expired'));
        $result3=$query3->fetchColumn();
        $result['TOTAL'] = $result0;
        $result['PAID'] = $result1;
        $result['PENDING'] = $result2;
        $result['FAILED'] = $result3;
        return $result;
    }
    public function viewTimeLine_Dashboard($agentid,$fromDate,$toDate){
        $query0 = parent::prepare("SELECT tb2.id,tb2.trans_date,tb2.trans_time,tb1.client_name,tb1.trans_title,tb2.oncard_name,tb2.status,tb2.card_bank,tb2.card_country,tb1.price,tb1.currency from vpcpay_payment_trans AS tb1 INNER JOIN vpcpay_trans_att AS tb2 ON tb1.refNun = tb2.ref_num WHERE tb1.agent_id = :id AND tb2.notify_stat <> :stat AND tb2.trans_date > :fDate AND tb2.trans_date <= :tDate ORDER BY tb2.trans_date DESC");
        $query0->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>0));
        $result0=$query0->fetchAll(PDO::FETCH_ASSOC);
        return $result0;
    }
    public function agentCalendar($id,$agentid,$title,$fromDate,$toDate,$color,$attr,$status,$description,$checkID){
        $checkQ = parent::prepare("SELECT count(*) FROM vpcpay_calendar WHERE id = :id");
        $checkQ->execute(array(':id'=>$id));
        $checkRes=$checkQ->fetchColumn();
        if ($checkRes > 0 ) {
            $query = parent::prepare("UPDATE vpcpay_calendar SET title=:title,description=:content,start=:fDate,end=:tDate,backgroundColor=:color,status=:stat WHERE id=:id");
            $query->execute(array(':id'=>$id,
                ':title'=>$title,
                ':content'=>$description,
                ':fDate'=>$fromDate,
                ':tDate'=>$toDate,
                ':color'=>$color,
                ':stat'=>$status));
            $query0 = true;
        }
        else {
            $query0 = parent::prepare("INSERT INTO vpcpay_calendar(id,agent_id,title,description,start,end,backgroundColor,allDay,status,check_id) VALUES(:id,:agentId,:title,:content,:fDate,:tDate,:color,:attr,:stat,:checkID)");
            $query0->execute(array(':id'=>$id,
                ':agentId'=>$agentid,
                ':title'=>$title,
                ':content'=>$description,
                ':fDate'=>$fromDate,
                ':tDate'=>$toDate,
                ':color'=>$color,
                ':attr'=>$attr,
                ':stat'=>$status,
                ':checkID'=>$checkID));
        }

        return $query0;
    }
    public function updateVisaCalendar($fromDate,$checkID){
        $query = parent::prepare("UPDATE vpcpay_calendar SET start=:fDate WHERE check_id=:checkId");
        $query->execute(array(':fDate'=>$fromDate,
            ':checkId'=>$checkID));
        $query0 = true;
        return $query0;
    }
    public function deleteCalendarEvent($id){
        $checkQ = parent::prepare("DELETE FROM vpcpay_calendar WHERE id = :id");
        $checkQ->execute(array(':id'=>$id));
        return $checkQ;
    }
    public function agentCalendarView($id){
        $query = parent::prepare("SELECT id,start,end,title,description,allDay,backgroundColor from vpcpay_calendar  WHERE agent_id =:id");
        $query->execute(array(':id'=>$id));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}