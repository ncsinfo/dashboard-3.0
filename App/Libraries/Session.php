<?php
namespace App\Libraries;

class Session {
	
	private $sessionKeys = array('theID','whosIN','TimeIN');
	private $counter = 0;
	public function __construct(){
		session_start();
		date_default_timezone_set('Asia/Dubai');// setting the time zone
		// check if session is set 
		if(!isset($_SESSION['user'])){
			echo "Session Not Set";
			//header('location:/login');
		}
		else if(empty($_SESSION['user'])){
				// Unset all of the session variables.
				$_SESSION = array();
				// If it's desired to kill the session, also delete the session cookie.
				// Note: This will destroy the session, and not just the session data!
				if (ini_get("session.use_cookies")) {
				    $params = session_get_cookie_params();
				    setcookie(session_name(), '', time() - 42000,
				        $params["path"], $params["domain"],
				        $params["secure"], $params["httponly"]
				    );
				}
				session_destroy();
				header('location:/login');
		}
		else if(isset($_SESSION)){
			$sessionArr = $_SESSION['user'];
			//if isset check the session keys and the login time if login time is less than the time logout the user and delete session
			foreach($this->sessionKeys as $key){
				if(array_key_exists($key, $sessionArr)){
					$this->counter++;
					if($this->counter = 3){
						// check the timeIN if less than 30 minutes of activity logout the client.
						$loginTime = $_SESSION['user']['TimeIN'];
						if($loginTime+(3600*9)  < time()){
							// Unset all of the session variables.
							$_SESSION = array();
							// If it's desired to kill the session, also delete the session cookie.
							// Note: This will destroy the session, and not just the session data!
							if (ini_get("session.use_cookies")) {
							    $params = session_get_cookie_params();
							    setcookie(session_name(), '', time() - 42000,
							        $params["path"], $params["domain"],
							        $params["secure"], $params["httponly"]
							    );
							}
							session_destroy();
							header('location:index.php');
						}else{
							$this->RegenConnection($loginTime); // regenerate Time in
						}
					}
				}else{
					$_SESSION = array();
					// If it's desired to kill the session, also delete the session cookie.
					// Note: This will destroy the session, and not just the session data!
					if (ini_get("session.use_cookies")) {
					    $params = session_get_cookie_params();
					    setcookie(session_name(), '', time() - 42000,
					        $params["path"], $params["domain"],
					        $params["secure"], $params["httponly"]
					    );
					}
					session_destroy();
					header('location:index.php');
				} 
			}
		}
	}

	private function RegenConnection($timeIn){
		if($timeIn !=null || $timeIn == ''){
		   date_default_timezone_set('Asia/Dubai');// setting the time zone
		   $_SESSION['user']['TimeIN'] = time();
		}else{
			$_SESSION = array();
					// If it's desired to kill the session, also delete the session cookie.
					// Note: This will destroy the session, and not just the session data!
			if (ini_get("session.use_cookies")) {
			    $params = session_get_cookie_params();
			    setcookie(session_name(), '', time() - 42000,
			        $params["path"], $params["domain"],
			        $params["secure"], $params["httponly"]
			    );
			}
			session_destroy();
			header('location:index.php');
		}
	}
	
}