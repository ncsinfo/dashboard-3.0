<?php
namespace App\Libraries;

use App\Libraries\Database;
use \PDO;

class Helper extends Database{
	
	public function initialTransinfo($cardName,$cardBank,$cardCountry,$clientip,$ref,$agent){
		$query=parent::prepare("INSERT INTO vpcpay_trans_att(onCard_name,card_bank,card_country,client_ip,ref_num,agent_id,notify_stat) VALUES(:cardName,:cardBank,:cardCountry,:clientip,:ref,:agent,:notify)");
		$query->execute(array(
			':cardName' => $cardName,
			':cardBank' => $cardBank,
			':cardCountry'=>$cardCountry,
			':clientip'=>$clientip,
			':ref'=>$ref,
			':agent'=>$agent,
            ':notify'=>0
			));
		return parent::lastInsertId();
	}

	public function sanitizeInputs(Array $inputs){
		$returnARR ='';
		foreach($inputs as $k => $v){
			$returnARR[$k] = filter_var($v,FILTER_SANITIZE_STRING);
		}
		return $returnARR;
	}
}