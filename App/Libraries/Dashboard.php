<?php
namespace App\Libraries;

use App\Libraries\Database;
use \PDO;

class Dashboard extends Database {
	public function dashboard_view($agentId){
        date_default_timezone_set("Asia/Dubai");
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime($endDate. '-15 days'));
        $startTimeLineDate = date('Y-m-d', strtotime($endDate. '-7 days'));
        $statQuery = $this->viewTransactions_Dashboard($agentId,$startDate,$endDate);
        $timeLineQuery = $this->viewTimeLine_Dashboard($agentId,$startTimeLineDate,$endDate);
        $paidRate = ($statQuery['PAID'] / $statQuery['TOTAL']) * 100;
        $paidRate = round($paidRate);
        $pendingRate = ($statQuery['PENDING'] / $statQuery['TOTAL']) * 100;
        $pendingRate = round($pendingRate);
        $failedRate = ($statQuery['FAILED'] / $statQuery['TOTAL']) * 100;
        $failedRate = round($failedRate);
        $timelineView = array();
        $timeLineData = array();
        while($startTimeLineDate != $endDate) {
            $cnt = 0;
            $timeLineData[$endDate] = array();
            foreach ($timeLineQuery as $key => $val) {
                foreach ($val as $k => $v) {
                    if ($v==$endDate){
                        array_push($timeLineData[$endDate],$timeLineQuery[$key]);
                        $cnt = $cnt + 1;
                    }
                }
            }
            $endDate = date('Y-m-d', strtotime($endDate. '-1 days'));
        }
        $timelineView['STAT']['TOTAL'] = $statQuery['TOTAL'];
        $timelineView['STAT']['PAID'] = $statQuery['PAID'];
        $timelineView['STAT']['PA_RATE'] = $paidRate.'% in the last 15 Days';
        $timelineView['STAT']['PENDING'] = $statQuery['PENDING'];
        $timelineView['STAT']['PE_RATE'] = $pendingRate.'% in the last 15 Days';
        $timelineView['STAT']['FAILED'] = $statQuery['FAILED'];
        $timelineView['STAT']['FA_RATE'] = $failedRate.'% in the last 15 Days';
        $timelineViewData = array();
        $dateColor = array("bg-red","bg-blue","bg-green","bg-yellow","bg-aqua","bg-maroon","bg-orange");
        foreach ($timeLineData as $k => $v) {
            if (!empty($v)) { $sDate = date_create($k);
                $timelineView[date_format($sDate,"Y-m-d")]['set_date'] = date_format($sDate,"M. d, Y");
                $timelineView[date_format($sDate,"Y-m-d")]['set_color'] = $dateColor[rand(0,6)];
                foreach ($v as $k1 => $v1) {
                    $lineDate = $v1['trans_date'].' '.$v1['trans_time'];
                    $lineDate = date_create($lineDate);
                    $lineDate = date_format($lineDate,"h:i A");
                    $curr = $v1['currency'];
                    if ($curr=='us'){$curr='$ '.number_format($v1['price'], 2, '.', ',');}else {$curr='AED '.number_format($v1['price'], 2, '.', ',');}
                    $timelineViewData['set_time'] = $lineDate;
                    $timelineViewData['client_name'] = $v1['client_name'];
                    $timelineViewData['price'] = $v1['trans_title'].' - '.$curr;
                    $timelineViewData['status'] = $v1['status'];
                    $timelineViewData['oncard_name'] = $v1['oncard_name'];
                    $timelineViewData['card_bank'] = $v1['card_bank'];
                    $timelineViewData['card_bank'] = $v1['card_bank'];
                    $timelineViewData['card_country'] = $v1['card_country'];
                    $timelineViewData['id'] = $v1['id'];
                    $timelineView[date_format($sDate,"Y-m-d")]['data'][] = $timelineViewData;
                }
            }
        }
        return $timelineView;
    }
    private function viewTransactions_Dashboard($agentid,$fromDate,$toDate){
        $query0 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND date_create > :fDate AND date_create <= :tDate");
        $query0->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate));
        $result0=$query0->fetchColumn();
        $query1 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND status = :stat AND date_create > :fDate AND date_create <= :tDate");
        $query1->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>'Paid'));
        $result1=$query1->fetchColumn();
        $query2 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND status = :stat AND date_create > :fDate AND date_create <= :tDate");
        $query2->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>'Pending'));
        $result2=$query2->fetchColumn();
        $query3 = parent::prepare("SELECT count(*) from vpcpay_payment_trans WHERE agent_id = :id AND status = :stat AND date_create >= :fDate AND date_create <= :tDate");
        $query3->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>'Expired'));
        $result3=$query3->fetchColumn();
        $result['TOTAL'] = $result0;
        $result['PAID'] = $result1;
        $result['PENDING'] = $result2;
        $result['FAILED'] = $result3;
        return $result;
    }
    private function viewTimeLine_Dashboard($agentid,$fromDate,$toDate){
        $query0 = parent::prepare("SELECT tb2.id,tb2.trans_date,tb2.trans_time,tb1.client_name,tb1.trans_title,tb2.oncard_name,tb2.status,tb2.card_bank,tb2.card_country,tb1.price,tb1.currency from vpcpay_payment_trans AS tb1 INNER JOIN vpcpay_trans_att AS tb2 ON tb1.refNun = tb2.ref_num WHERE tb1.agent_id = :id AND tb2.notify_stat <> :stat AND tb2.trans_date > :fDate AND tb2.trans_date <= :tDate ORDER BY tb2.trans_date DESC");
        $query0->execute(array(':id'=>$agentid,
            ':fDate'=>$fromDate,
            ':tDate'=>$toDate,
            ':stat'=>0));
        $result0=$query0->fetchAll(PDO::FETCH_ASSOC);
        return $result0;
    }
}