<?php 

namespace App\Libraries;

use \PDO;

class ChartDatabase extends PDO {
	
	protected $username = 'root';
	protected $serverName ='localhost';
	protected $dbname = 'wp2';
	protected $password = '';
	protected $dsn = 'mysql:dbname=wp2;host=localhost';
	public function __construct(){
		try {
			parent:: __construct($this->dsn,$this->username,$this->password);
			// set the PDO error mode to exception
			$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//echo "Connected successfully"; 
		}
		catch(PDOException $e)
		{
			return "Connection failed: " . $e->getMessage();
		}
	}
	
}