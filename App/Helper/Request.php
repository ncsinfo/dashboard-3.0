<?php 
namespace App\Helper;

use App\Libraries\Database;
use \PDO;
use App\Models\GenerateToken;
use App\Models\Query;
use App\Models\Users;
use App\Models\Notifications;
use App\Libraries\Validation;
use App\Libraries\Functions;
use App\Libraries\Dashboard;

/*
include_once('../app/class_db_query.php');
include_once('../app/class_db_functions.php');
include_once('../app/class_db_notification.php');
include_once('../app/class_db_dashboard.php');
include_once('../app/class_token_gen.php');
include_once('../app/class_user.php');
include_once('../app/class_visa_record.php');
include_once('../app/class_contacts.php');
*/
class Request extends Database {
	
	public static function request(){
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
		switch ($_POST['requests']) {
			case 'tokengen':
				parse_str($_POST['formDate'],$output);
				echo GenerateToken::toks($output);
				break;
				
			case 'refreshlistofgeneratedlinks':
				session_start();
				$query = new Query;
				$transactions = $query->getTransactions($_SESSION['user']['whosIN']);
				//$transactions = $query->getTransactions(2);
				echo json_encode($transactions);
				break;
				
			case 'deletepaymentLink':
				$query = new Query;
				$delete = $query->delpayRec($_POST['token']);
				echo $delete;
				break;	
				
			case 'createnewuser':
				parse_str($_POST['form'],$fromForm);
				$newUser = new Validation;
				$createUser = $newUser->createNewUser($fromForm);
				if(array_key_exists('pass',$createUser)){
					$Newpass = new userPass();
					$fromForm['pass'] = $Newpass->myhash($fromForm['pass']);
					$addUser = new addUser();
					if($addUser->add($fromForm)){
						echo true;
					}
					else{
						echo false;
					}
				}else{
					echo json_encode($createUser);
				}
				break;	
				
			case 'dateSortAgentPage':
				$newData = new Functions;
				$newQ0 = $newData->getClientTransactionAttDate($_POST['sortAgent'], $_POST['dateField'],$_POST['searchTxt']);
				//	$newQ = $newData->getClientTransactionAttDate($_SESSION['user']['whosIN'], $_POST['dateField']);
				echo json_encode($newQ0);
				break;

				//Sort payment link table by search
			case 'searchAgentPaymentLink':
				session_start();
				$newData = new Functions;
				$newQ = $newData->getTransactions_search($_POST['sortAgent'], $_POST['searchTxt']);
				echo json_encode($newQ);
				break;

			case 'agentSortAgentPage':
				$newData = new Functions;
				$newQ = $newData->getClientTransactionAttDate($_POST['sortAgent'], $_POST['dateField'],'');
				//	  $newQ = $newData->getClientTransactionAttDate($_SESSION['user']['whosIN'], $_POST['dateField']);
				echo json_encode($newQ);
				//print_r($newQ);
				break;

				//Sort payment link table by date
			case 'dateSortAgentPaymentLink':
				session_start();
				$newData = new Functions;
				$newQ = $newData->getTransactions_wDate($_POST['sortAgent'], $_POST['dateField']);
				echo json_encode($newQ);
				break;

				//Generate Reference Number
			case 'createReferenceNum':
				$newQuery = new Query;
				$name = $_POST['name'];
				$pre = substr($name, 0,3);
				$day = date('YmdHis');
				echo $pre.'-'.$day;
				break;
				
			case 'dashboardContent':
				$newQ = new Dashboard;
				$agentID = $_POST['agentID'];
				$DashQuery = $newQ->dashboard_view($agentID);
				echo json_encode($DashQuery);
				break;
				
			case 'searchNotification':
				$newNotify = new Notifications;
				$agentID = $_POST['agentID'];
				if ($_POST['typeNotify']==1){$agentN = $newNotify->checkNotification($agentID);}
				else if ($_POST['typeNotify']==2){$agentN = $newNotify->checkVisaNotification($agentID);}
				else if ($_POST['typeNotify']==3){$agentN = $newNotify->checkVisaNotification2($agentID);}
				else {$agentN = $newNotify->notifyTransactions_search($agentID);}
				$s = array();
				foreach($agentN as $key => $val) {
					array_push($s,$val);
				}
				echo json_encode($s);
				break;
				
			case 'updateNotification':
				$newQ = new Notifications;
				if ($_POST['typeNotify']==1){
					$agentID = $_POST['agentID'];
					$agentN = $newQ->updateNotification($agentID);
				}
				else if ($_POST['typeNotify']==2){
					$agentID = $_POST['agentID'];
					$agentN = $newQ->updateVisaNotification($agentID);
				}
				else if ($_POST['typeNotify']==3){
					$notifyID = $_POST['notifyID'];
					$agentN = $newQ->updateVisaNotification2($notifyID);
				}
				else if ($_POST['typeNotify']==4){
					$notifyID = $_POST['notifyID'];
					$agentN = $newQ->updateNotification3($notifyID);
				}
				else {
					$notifyID = $_POST['notifyID'];
					$agentN = $newQ->updateNotification2($notifyID);
				}
				echo json_encode($agentN);
				break;
				
			case 'getVisaByID':
				session_start();
				$visarecord = new Notifications;
				$visaID = $_POST['visaID'];
				echo json_encode($visarecord->visaRequestByID($visaID));
				break;
			case 'popClientData':
				$newQ = new Query;
				$tokClient = $_POST['clientToken'];
				$clientN = $newQ->populateClient($tokClient);
				$s = array();
				foreach($clientN as $key => $val) {
					array_push($s,$val);
				}
				echo json_encode($s);
				break;
				
			case 'invoiceData':
				$newQ = new Functions;
				$transID = $_POST['transID'];
				$clientN = $newQ->agentTransactionDetails($transID);
				echo json_encode($clientN);
				break;
				
			case 'generateReport':
				$newQ = new Functions;
				$agentID = $_POST['agentID'];
				$dateFrom = $_POST['dateFrom'];
				$dateTo = $_POST['dateTo'];
				$clientN = $newQ->generateAgentReport($agentID,$dateFrom,$dateTo);
				$finalArr = "";
				$valAed = array();
				$valUsd = array();
				$usd = "";
				$total = array();
				$varDate = '';
				$varAmount = 0;
				foreach($clientN['AED'] as $v){
					$date = $v['trans_date'];
					$amnt = $v['price'];
					if (array_key_exists($date,$valAed)) {
						$tempVal = $valAed[$date];
						$tempVal = floatval($tempVal) + floatval($amnt);
						$valAed[$date] = floatval($tempVal);
					}else {
						$valAed[$date] = floatval($amnt);
					}
				}
				foreach($clientN['USD'] as $v){
					$date = $v['trans_date'];
					$amnt = $v['price'];
					if (array_key_exists($date,$valUsd)) {
						$tempVal = $valUsd[$date];
						$tempVal = floatval($tempVal) + floatval($amnt);
						$valUsd[$date] = floatval($tempVal);
					}else {
						$valUsd[$date] = floatval($amnt);
					}
				}
				$dateVal = array();
				$date1 = date_create($dateFrom);
				$date2 = date_create($dateTo);
				$finalAmount = array();
				$aedVal = array();
				$usdVal = array();
				$dateVal = array();
				$totalAmnt = array();
				$s = array();
				$totalAED = 0;
				$totalUSD = 0;
				while ($date1 <= $date2) {
					$amount = 0;
					$mDate = date_format($date1,"Y-m-d");
					if (array_key_exists($mDate,$valAed)) {
						//$aedVal[$mDate] =$valAed[$mDate];
						$aedVal[] =$valAed[$mDate];
						$totalAED = $totalAED + $valAed[$mDate];
					}else {
						//$aedVal[$mDate] = 0;
						$aedVal[] = 0;
					}
					if (array_key_exists($mDate,$valUsd)) {
						//$usdVal[$mDate] =$valUsd[$mDate];
						$usdVal[] =$valUsd[$mDate];
						$totalUSD = $totalUSD + $valUsd[$mDate];
					}else {
						//$usdVal[$mDate] = 0;
						$usdVal[] = 0;
					}
					$dateVal[] = date_format($date1,"m/d/Y");
					date_add($date1,date_interval_create_from_date_string("1 days"));
				}
				$totalAmnt['AED'] = $totalAED;
				$totalAmnt['USD'] = $totalUSD;
				$valAmount = array();
				$valAmount['AED'] = $aedVal;
				$valAmount['USD'] = $usdVal;
				$valAmount['DATE'] = $dateVal;
				$valAmount['AMT'] = $totalAmnt;
				$valAmount['CNT'] = $clientN['CNT']['count(id)'];
				$valAmount['AGT'] = $clientN['AGT']['full_name'];
				echo json_encode($valAmount);
				break;
				
			case 'agentData':
				$newQ = new Functions;
				$dateFrom = $_POST['dateFrom'];
				$dateTo = $_POST['dateTo'];
				$clientN = $newQ->agentDataReport($dateFrom,$dateTo);
				echo json_encode($clientN);
				break;
				
			case 'agentTransReport':
				$newQ = new Functions;
				$dateFrom = $_POST['dateFrom'];
				$dateTo = $_POST['dateTo'];
				$agentID = $_POST['agentID'];
				$clientN = $newQ->agentTransactionReport($agentID,$dateFrom,$dateTo);
				echo json_encode($clientN);
				break;
				
			case 'changePass':
				$hashit = new Users;
				$newPAss = $hashit->myhash($_POST['newpass']);
				$newQ = new Query;
				$savenewPAss = $newQ->changePass($_POST['userid'],$newPAss);
				echo $savenewPAss;
				break;
				
			case 'addUserCap' :
				parse_str($_POST['cap'],$capabilities);
				$newQ = new Query;
				$updateCap = $newQ->updateUsercap($_POST['userid'],serialize($capabilities)); 
				echo $updateCap;
				break;
				
			case 'retUserCap':
				$newQ = new Query;
				$retUserCap = $newQ->getRegUserCap($_POST['userid']);
				echo empty($retUserCap['user_capability']) ? 'false' : json_encode(unserialize($retUserCap['user_capability']));
				break;
			/*	 
			case 'addVisa':
				session_start();
				parse_str($_POST['visaForm'],$newVisaDetails);
				$newVisa = new VisaUae();
				$addNewVisa = $newVisa->addNEwVisaRecord($newVisaDetails,$_SESSION['user']['whosIN']);
				echo $addNewVisa; 
				break;
			case 'pullvisarecord':
				session_start();
				$visarecord = new VisaUae();
				$pullRec = $visarecord->getVisaRecDet($_SESSION['user']['whosIN'],$_POST['rec']);
				echo json_encode($pullRec);
				break;
			case 'CalculatorDate':
				
				parse_str($_POST['formData'],$formData);
				if($formData['calcOption'] === 'add'){
					$date = new DateTime($formData['dateSelected']);
					$Days = (int)$formData['daysToCalculate']; 
					$date->add(new DateInterval('P'.$Days.'D'));
					echo $date->format('F d, Y');
				}
				else{
					$date = new DateTime($formData['dateSelected']);
					$Days = (int)$formData['daysToCalculate']; 
					$date->sub(new DateInterval('P'.$Days.'D'));
					echo $date->format('F d, Y');
				}
				break;
			case 'updateVisaRecords':
				parse_str($_POST['formDetails'],$formDetails);
				$visarecord = new VisaUae();
				echo $visarecord->UpdateVisaRecord($formDetails,$_POST['rec']);
				break;
			case 'getVisaRecords':
				session_start();
				$visarecord = new VisaUae();
				echo json_encode($visarecord->getRecordForEditPage($_SESSION['user']['whosIN']));
				break;
			case 'getVisaByAgent':
				session_start();
				$visarecord = new VisaUae();
				echo json_encode($visarecord->visaRequestByagent($_SESSION['user']['whosIN']));
				break;
			case 'addContacts':
				parse_str($_POST['addForm'], $contacts);
				$AddContact = new Contacts();
				$addToContacts = $AddContact->addRecordRetID($contacts); 
				if($addToContacts){
					$visa = new VisaUae();
					echo $visa->updateVisaGuarantor($_POST['recordID'],$addToContacts);
				}else{
					echo false;
				}
				break;
			case 'addGurantorToVisaRecords':
				parse_str($_POST['addForm'], $contactid);
				$visa = new VisaUae();
				echo $visa->updateVisaGuarantor($_POST['recordID'],$contactid['selectedID']);
				break;
			case 'listGuarantor':
				$guarantor = new Contacts();
				echo json_encode($guarantor->getGuarantors($_POST['guaArr']));
				break;
			case 'detachedGuarantor':
				$visa = new VisaUae();
				echo $visa->detachedGuarantor($_POST['recordID'],$_POST['contactID']);
				//echo $_POST['contactID'].'<br>'.$_POST['recordID'];
				break;
			case 'deleteVisaRec':
				$visa = new VisaUae();
				echo $visa->visaRecordDel($_POST['visaRecId']);
				break;
			case 'calculateExitDate':
				$date = new DateTime($_POST['entryDate']);
				$Days = (int)$_POST['visatype'] - 1; 
				$date->add(new DateInterval('P'.$Days.'D'));
				echo $date->format('Y-m-d');
				break;*/
			default:
				echo false;
				break;	
		}
	}
	}
	
	
	/*
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		switch ($_POST['requests']) {
			case 'tokengen':
				parse_str($_POST['formDate'],$output);
				$token = new genToken();
				echo $token->toks($output);
				break;

			case 'refreshlistofgeneratedlinks':
				session_start();
				$query = new dbq();
				$transactions = $query->getTransactions($_SESSION['user']['whosIN']);
				echo json_encode($transactions);
				break;

			case 'deletepaymentLink':
				$query = new dbq();
				$delete = $query->delpayRec($_POST['token']);
				echo $delete;
				break;

			case 'createnewuser':
				parse_str($_POST['form'],$fromForm);
				$newUser = new validateInputs();
				$createUser = $newUser->createNewUser($fromForm);
				if(array_key_exists('pass',$createUser)){
					$Newpass = new userPass();
					$fromForm['pass'] = $Newpass->myhash($fromForm['pass']);
					$addUser = new addUser();
					if($addUser->add($fromForm)){
						echo true;
					}
					else{
						echo false;
					}
				}else{
					echo json_encode($createUser);
				}
				break;

			case 'dateSortAgentPage':
				$newData = new fdb();
				$newQ0 = $newData->getClientTransactionAttDate($_POST['sortAgent'], $_POST['dateField'],$_POST['searchTxt']);
				//	$newQ = $newData->getClientTransactionAttDate($_SESSION['user']['whosIN'], $_POST['dateField']);
				echo json_encode($newQ0);
				break;

				//Sort payment link table by search
			case 'searchAgentPaymentLink':
				session_start();
				$newData = new fdb();
				$newQ = $newData->getTransactions_search($_POST['sortAgent'], $_POST['searchTxt']);
				echo json_encode($newQ);
				break;

			case 'agentSortAgentPage':
				$newData = new fdb();
				$newQ = $newData->getClientTransactionAttDate($_POST['sortAgent'], $_POST['dateField'],'');
				//	  $newQ = $newData->getClientTransactionAttDate($_SESSION['user']['whosIN'], $_POST['dateField']);
				echo json_encode($newQ);
				//print_r($newQ);
				break;


				//Sort payment link table by date
			case 'dateSortAgentPaymentLink':
				session_start();
				$newData = new fdb();
				$newQ = $newData->getTransactions_wDate($_POST['sortAgent'], $_POST['dateField']);
				echo json_encode($newQ);
				break;


				//Generate Reference Number
			case 'createReferenceNum':
				$newQuery = new dbq();
				$name = $_POST['name'];
				$pre = substr($name, 0,3);
				$day = date('YmdHis');
				echo $pre.'-'.$day;
				break;
			case 'dashboardContent':
				$newQ = new dashboard_db();
				$agentID = $_POST['agentID'];
				$DashQuery = $newQ->dashboard_view($agentID);
				echo json_encode($DashQuery);
				break;
			case 'searchNotification':
				$newNotify = new ndb();
				$agentID = $_POST['agentID'];
				if ($_POST['typeNotify']==1){$agentN = $newNotify->checkNotification($agentID);}
				else if ($_POST['typeNotify']==2){$agentN = $newNotify->checkVisaNotification($agentID);}
				else if ($_POST['typeNotify']==3){$agentN = $newNotify->checkVisaNotification2($agentID);}
				else {$agentN = $newNotify->notifyTransactions_search($agentID);}
				$s = array();
				foreach($agentN as $key => $val) {
					array_push($s,$val);
				}
				echo json_encode($s);
				break;
			case 'updateNotification':
				$newQ = new ndb();
				if ($_POST['typeNotify']==1){
					$agentID = $_POST['agentID'];
					$agentN = $newQ->updateNotification($agentID);
				}
				else if ($_POST['typeNotify']==2){
					$agentID = $_POST['agentID'];
					$agentN = $newQ->updateVisaNotification($agentID);
				}
				else if ($_POST['typeNotify']==3){
					$notifyID = $_POST['notifyID'];
					$agentN = $newQ->updateVisaNotification2($notifyID);
				}
				else if ($_POST['typeNotify']==4){
					$notifyID = $_POST['notifyID'];
					$agentN = $newQ->updateNotification3($notifyID);
				}
				else {
					$notifyID = $_POST['notifyID'];
					$agentN = $newQ->updateNotification2($notifyID);
				}
				echo json_encode($agentN);
				break;
			case 'getVisaByID':
				session_start();
				$visarecord = new ndb();
				$visaID = $_POST['visaID'];
				echo json_encode($visarecord->visaRequestByID($visaID));
				break;
			case 'popClientData':
				$newQ = new dbq();
				$tokClient = $_POST['clientToken'];
				$clientN = $newQ->populateClient($tokClient);
				$s = array();
				foreach($clientN as $key => $val) {
					array_push($s,$val);
				}
				echo json_encode($s);
				break;
			case 'invoiceData':
				$newQ = new fdb();
				$transID = $_POST['transID'];
				$clientN = $newQ->agentTransactionDetails($transID);
				echo json_encode($clientN);
				break;
			case 'generateReport':
				$newQ = new fdb();
				$agentID = $_POST['agentID'];
				$dateFrom = $_POST['dateFrom'];
				$dateTo = $_POST['dateTo'];
				$clientN = $newQ->generateAgentReport($agentID,$dateFrom,$dateTo);
				$finalArr = "";
				$valAed = array();
				$valUsd = array();
				$usd = "";
				$total = array();
				$varDate = '';
				$varAmount = 0;
				foreach($clientN['AED'] as $v){
					$date = $v['trans_date'];
					$amnt = $v['price'];
					if (array_key_exists($date,$valAed)) {
						$tempVal = $valAed[$date];
						$tempVal = floatval($tempVal) + floatval($amnt);
						$valAed[$date] = floatval($tempVal);
					}else {
						$valAed[$date] = floatval($amnt);
					}
				}
				foreach($clientN['USD'] as $v){
					$date = $v['trans_date'];
					$amnt = $v['price'];
					if (array_key_exists($date,$valUsd)) {
						$tempVal = $valUsd[$date];
						$tempVal = floatval($tempVal) + floatval($amnt);
						$valUsd[$date] = floatval($tempVal);
					}else {
						$valUsd[$date] = floatval($amnt);
					}
				}
				$dateVal = array();
				$date1 = date_create($dateFrom);
				$date2 = date_create($dateTo);
				$finalAmount = array();
				$aedVal = array();
				$usdVal = array();
				$dateVal = array();
				$totalAmnt = array();
				$s = array();
				$totalAED = 0;
				$totalUSD = 0;
				while ($date1 <= $date2) {
					$amount = 0;
					$mDate = date_format($date1,"Y-m-d");
					if (array_key_exists($mDate,$valAed)) {
						//$aedVal[$mDate] =$valAed[$mDate];
						$aedVal[] =$valAed[$mDate];
						$totalAED = $totalAED + $valAed[$mDate];
					}else {
						//$aedVal[$mDate] = 0;
						$aedVal[] = 0;
					}
					if (array_key_exists($mDate,$valUsd)) {
						//$usdVal[$mDate] =$valUsd[$mDate];
						$usdVal[] =$valUsd[$mDate];
						$totalUSD = $totalUSD + $valUsd[$mDate];
					}else {
						//$usdVal[$mDate] = 0;
						$usdVal[] = 0;
					}
					$dateVal[] = date_format($date1,"m/d/Y");
					date_add($date1,date_interval_create_from_date_string("1 days"));
				}
				$totalAmnt['AED'] = $totalAED;
				$totalAmnt['USD'] = $totalUSD;
				$valAmount = array();
				$valAmount['AED'] = $aedVal;
				$valAmount['USD'] = $usdVal;
				$valAmount['DATE'] = $dateVal;
				$valAmount['AMT'] = $totalAmnt;
				$valAmount['CNT'] = $clientN['CNT']['count(id)'];
				$valAmount['AGT'] = $clientN['AGT']['full_name'];
				echo json_encode($valAmount);
				break;
			case 'agentData':
				$newQ = new fdb();
				$dateFrom = $_POST['dateFrom'];
				$dateTo = $_POST['dateTo'];
				$clientN = $newQ->agentDataReport($dateFrom,$dateTo);
				echo json_encode($clientN);
				break;
			case 'agentTransReport':
				$newQ = new fdb();
				$dateFrom = $_POST['dateFrom'];
				$dateTo = $_POST['dateTo'];
				$agentID = $_POST['agentID'];
				$clientN = $newQ->agentTransactionReport($agentID,$dateFrom,$dateTo);
				echo json_encode($clientN);
				break;
			case 'changePass':
				$hashit = new userPass();
				$newPAss = $hashit->myhash($_POST['newpass']);
				$newQ = new dbq();
				$savenewPAss = $newQ->changePass($_POST['userid'],$newPAss);
				echo $savenewPAss;
				break;
			case 'addUserCap' :
				parse_str($_POST['cap'],$capabilities);
				$newQ = new dbq();
				$updateCap = $newQ->updateUsercap($_POST['userid'],serialize($capabilities)); 
				echo $updateCap;
				break;
			case 'retUserCap':
				$newQ = new dbq();
				$retUserCap = $newQ->getRegUserCap($_POST['userid']);
				echo empty($retUserCap['user_capability']) ? 'false' : json_encode(unserialize($retUserCap['user_capability']));
				break;
			case 'addVisa':
				session_start();
				parse_str($_POST['visaForm'],$newVisaDetails);
				$newVisa = new VisaUae();
				$addNewVisa = $newVisa->addNEwVisaRecord($newVisaDetails,$_SESSION['user']['whosIN']);
				echo $addNewVisa; 
				break;
			case 'pullvisarecord':
				session_start();
				$visarecord = new VisaUae();
				$pullRec = $visarecord->getVisaRecDet($_SESSION['user']['whosIN'],$_POST['rec']);
				echo json_encode($pullRec);
				break;
			case 'CalculatorDate':
				
				parse_str($_POST['formData'],$formData);
				if($formData['calcOption'] === 'add'){
					$date = new DateTime($formData['dateSelected']);
					$Days = (int)$formData['daysToCalculate']; 
					$date->add(new DateInterval('P'.$Days.'D'));
					echo $date->format('F d, Y');
				}
				else{
					$date = new DateTime($formData['dateSelected']);
					$Days = (int)$formData['daysToCalculate']; 
					$date->sub(new DateInterval('P'.$Days.'D'));
					echo $date->format('F d, Y');
				}
				break;
			case 'updateVisaRecords':
				parse_str($_POST['formDetails'],$formDetails);
				$visarecord = new VisaUae();
				echo $visarecord->UpdateVisaRecord($formDetails,$_POST['rec']);
				break;
			case 'getVisaRecords':
				session_start();
				$visarecord = new VisaUae();
				echo json_encode($visarecord->getRecordForEditPage($_SESSION['user']['whosIN']));
				break;
			case 'getVisaByAgent':
				session_start();
				$visarecord = new VisaUae();
				echo json_encode($visarecord->visaRequestByagent($_SESSION['user']['whosIN']));
				break;
			case 'addContacts':
				parse_str($_POST['addForm'], $contacts);
				$AddContact = new Contacts();
				$addToContacts = $AddContact->addRecordRetID($contacts); 
				if($addToContacts){
					$visa = new VisaUae();
					echo $visa->updateVisaGuarantor($_POST['recordID'],$addToContacts);
				}else{
					echo false;
				}
				break;
			case 'addGurantorToVisaRecords':
				parse_str($_POST['addForm'], $contactid);
				$visa = new VisaUae();
				echo $visa->updateVisaGuarantor($_POST['recordID'],$contactid['selectedID']);
				break;
			case 'listGuarantor':
				$guarantor = new Contacts();
				echo json_encode($guarantor->getGuarantors($_POST['guaArr']));
				break;
			case 'detachedGuarantor':
				$visa = new VisaUae();
				echo $visa->detachedGuarantor($_POST['recordID'],$_POST['contactID']);
				//echo $_POST['contactID'].'<br>'.$_POST['recordID'];
				break;
			case 'deleteVisaRec':
				$visa = new VisaUae();
				echo $visa->visaRecordDel($_POST['visaRecId']);
				break;
			case 'calculateExitDate':
				$date = new DateTime($_POST['entryDate']);
				$Days = (int)$_POST['visatype'] - 1; 
				$date->add(new DateInterval('P'.$Days.'D'));
				echo $date->format('Y-m-d');
				break;
			default:
				echo false;
				break;
		}
	} */

}
