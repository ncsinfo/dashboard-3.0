<?php
namespace App\Helper;

use App\Libraries\Database;
use \PDO;

class Validation extends Database {
	
	//Hashing
	public function myhash($password) {
	    return crypt($password, '$2a$10$'.$this->unique_salt());
	}

	private function unique_salt() {
	    return substr(sha1(mt_rand()),0,22);
	}

	public function check_password($hash, $password) {
	 
	    // first 29 characters include algorithm, cost and salt
	    // let's call it $full_salt
	    $full_salt = substr($hash, 0, 29);
	 
	    // run the hash function on $password
	    $new_hash = crypt($password, $full_salt);
	 
	    // returns true or false
	    return ($hash == $new_hash);
	}
	
	//ValidateInputs
	/* 
	*function to access the post data and process it
	*returns an associative array of errors or empty array
	*/
	public function createNewUser($inputs){
		$errors = array();
		$sani = $this->checkUserInput($inputs); // Sanitize inputs
		$validateDEmail = $this->validateEmail($sani['emailAdd']); // Validate Email
		$validatePass = $this->checkUserPass($sani['pass']); //validate pass
		$validateUserName = $this->check_for_username($sani['username']);
		$arrChecked = array('validateEmail'=>$validateDEmail,
							'validatePass'=>$validatePass,
							'validateUserName' => $validateUserName); 
		//start checking the values and throw errors 
		foreach ($arrChecked as $key => $action) {
		  	if($action){
		  		$errors[$key]=$action;
		  	}
		}
		if(count($errors)<=0){
			return $sani; 
		}else{
			return $errors;
		}  
	}

	// Sanitize user inputs returning an associative array with the same key 
	private function checkUserInput($userInputs){
		$SanitizeARR = array();
		if(is_array($userInputs) || is_object($userInputs)){
			foreach($userInputs as $k=>$v){
				switch ($k) {
					case 'emailAdd': // sanitize user email inputs
						$sanitizeEmail = filter_var($v, FILTER_SANITIZE_EMAIL);
						$SanitizeARR[$k] = $sanitizeEmail;
						break;
					case 'pass':
						$SanitizeARR[$k] = $v;
						break;
					default:
						$SanitizeARR[$k] = filter_var($v, FILTER_SANITIZE_STRING);
				}
			}
			return $SanitizeARR;
		}
		return false;
	}
		// function for validating and email
	private function validateEmail($email){
		$validate = filter_var($email, FILTER_VALIDATE_EMAIL);
		if(!$validate){
			$error = $this->errMsg('notValidEmail');
			return $error;
		}else if($validate){
			$checkEmail = $this->email_used($email);
			if($checkEmail){
				$error = $this->errMsg('emailUSed');
				return $error;
			}else{
				return false;
			}
		}
		
	}

	// function for checking if an email is already in used.
	private function email_used($email){
		$query = parent::prepare("SELECT count(email) from vpcpay_users where email = :email");
		$query->execute(array(':email'=>$email));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		if($result){
			return $result['count(email)'];
		}
	} 
	// function for checking if username is already in used.
	private function check_for_username($user){
		$query = parent::prepare("SELECT count(username) from vpcpay_users where username = :user");
		$query->execute(array(':user'=>$user));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		if($result['count(username)'] >=1){
			$error = $this->errMsg('userNameUsed');
			//return $result['count(email_add)'];
			return $error;
		}
	return false;
	} 
	//CheckPassWord
	private function checkUserPass($pass){
		$errors = array();
		$checkCaps = '/[A-Z]{1}/';
		$checkSpaces = '/\s/';
		$checkDigits = '/\d{1}/';
		$checkSpeChar = '/[!@#\$%\^&\*\(\)]/';
		$checkLength = '/\w{8}/';
		$ARRfilter = array('capital'=>$checkCaps,
						   'spaces'=>$checkSpaces,
						   'digits'=>$checkDigits,
						   'specialchar'=>$checkSpeChar,
						   'passbelow8' =>$checkLength );
		foreach ($ARRfilter as $key => $filter) {
			if($key === 'spaces' && preg_match($filter, $pass)){
				$errors[$key]=$this->errMsg((string)$key);
			}
			else if($key !== 'spaces' && !preg_match($filter, $pass)){
				$errors[$key]=$this->errMsg((string)$key);	
			}
		}
		return $errors;
	}
	// Collections of error messages.
	private function errMsg($response){
		$msg = array(
				'notValidEmail' => 'Email address is not valid',
				'passbelow8' => 'Your Password should be 8 Characters Long',
				'capital'  => 'Your Password should atleast has a capital letter',
				'digits' => 'Your Password should atleast has a Number',
				'usedUserName' => 'This User Name is already in used. Please try other one',
				'usedEmailAddress' => 'The email address you entered is already in used. Please try other one',
				'specialchar'=> 'Your Password should have atleast has a special characters.example(!@#)',
				'spaces'=> "Your Password should'nt contain a spaces",
				'emailUSed' => "Your Email is already registered. Please try another one",
				'userNameUsed'=>"User is not available"
			);
		if($msg[$response]){
			return $msg[$response];
		}else{
			return 'Error Occured';
		}
	}
	
}