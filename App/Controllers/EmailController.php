<?php
namespace App\Controllers;

use App\Controller;
use App\Models\Chart;
class EmailController {
	
	public function visa(){
		$arg['inquiry'] = Chart::inquiry();
		$arg['tour'] = Chart::tourApplication();
		$arg['visa'] = Chart::visaApplication();
		$arg['count'] = Chart::counter();
		return Controller::view('view/emails/visa', $arg);
	}
	
	public function tour(){
		$arg = "Hello World";
		return Controller::view('view/emails/tours', $arg);
	}
	
	public function package(){
		$arg = "Hello World";
		return Controller::view('view/emails/packages', $arg);
	}
	
	public function other(){
		$arg = "Hello World";
		return Controller::view('view/emails/others', $arg);
	}
}