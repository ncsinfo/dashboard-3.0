<?php 
namespace App\Controllers;

use App\Controller;

class PaymentController {
	
	public function index()
	{
		$arg = "Hello World";
		
		return Controller::view('view/payments/index', $arg);
		
	}
	
	public function transaction()
	{
		$arg = "Hello World";
		return Controller::view('view/payments/transactions', $arg);
		
	}
	
	public function reports()
	{
		$arg = "Hello World";
		return Controller::view('view/payments/reports', $arg);
		
	}
	
}