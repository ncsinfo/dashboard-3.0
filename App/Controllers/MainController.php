<?php

namespace App\Controllers;

use App\Controller;
use App\Models\Login;
use App\Models\Query;
class MainController {
	
	public function index()
	{
		
		$arg = "Hello World";
		return Controller::view('view/index', $arg);
		
	}
	
	public function login()
	{
		$arg = "Hello World";
		return Controller::view('view/login', $arg);
		
	}
	
	
	public function logout()
	{
		session_start();
		session_destroy();
		header( 'Location: /login' ) ;
	}

	
	
}