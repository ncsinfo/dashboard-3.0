<?php
namespace App\Models;

use App\Libraries\Database;
use \PDO;

class GenerateToken extends Database {
	
	public function toks($arr){
		$ran =  mt_rand();
		$shufEmail = str_shuffle($arr['email']);
		$ran2 =  uniqid($shufEmail);
		$url ='https://secure.southtravels.com/paynow.php?id='.$ran2;
		$SaveToken = GenerateToken::addTokentoDb($ran2);
		if($SaveToken > 0 ){
			if(GenerateToken::addTransaction($SaveToken,$arr)){
				return $url;
			}else{
				return false;
			}
		}
		return $url;
			
	}

	private static function addTokentoDb($token){
		session_start();
		$conn = new Database;
		$query = $conn->prepare('INSERT INTO vpcpay_url_token(token,attempts,allowed_attempts,agent_id,status) VALUES(:token,:attempts,:allowedAttempts,:agentID,:status)');
		$query->execute(array(
			':token' => $token,
			':attempts' => 0,
			':allowedAttempts' => 3,
			//':agentID' => $_SESSION['user']['whosIN'],
			':agentID' => 2,
			':status' => 1
			));
		return $conn->lastInsertId();
	}

	private function addTransaction($tokenID,$dataArr){
		$conn = new Database;
		date_default_timezone_set('Asia/Dubai');// setting the time zone
		$query= $conn->prepare('INSERT INTO vpcpay_payment_trans(trans_title,status,date_create,agent_id,client_name,price,tokenID,refNun,client_email,currency) VALUES(:trans_title,:status,:date_create,:agent_id,:client_name,:price,:tokenID,:refNun,:client_email,:currency)');
		$query->execute(array(
			':trans_title'=>$dataArr['payfor'],
			':status'=>'Pending',
			':date_create'=>date('Y-m-d'),
			//':agent_id'=> $_SESSION['user']['whosIN'],
			':agent_id'=> 2,
			':client_name'=>$dataArr['fullname'],
			':price'=>$dataArr['figure'],
			':tokenID'=>$tokenID,
			':refNun'=>$dataArr['refCode'],
			':client_email'=>$dataArr['email'],
			':currency'=>$dataArr['currency']
			));
		if(!$query){
			return $conn->errorInfo();
		}else{
			return true;
		}
	}
}