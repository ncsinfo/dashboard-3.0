<?php 
namespace App\Models;

use App\Libraries\ChartDatabase;
use \PDO;

class Chart extends ChartDatabase {
	
	public static function inquiry(){
		
		$conn = new ChartDatabase;
		if($conn){
			$qry = $conn->prepare("SELECT * FROM inquiry_to_dbs");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
	public static function tourApplication(){
		
		$conn = new ChartDatabase;
		if($conn){
			$qry = $conn->prepare("SELECT * FROM tour_applications");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
	public static function visaApplication(){
		
		$conn = new ChartDatabase;
		if($conn){
			$qry = $conn->prepare("SELECT * FROM visa_process");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
	public static function counter(){
		
		$conn = new ChartDatabase;
		if($conn){
			$qry = $conn->prepare("SELECT DISTINCT DATE(created_at) FROM inquiry_to_dbs");
			//$qry = $conn->prepare("SELECT COUNT(DISTINCT DATE(created_at)) as days FROM inquiry_to_dbs");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
	//CHART
	public static function generalInquiry($date){
		
		$conn = new ChartDatabase;
		if($conn){
			//$qry = $conn->prepare("SELECT DISTINCT DATE(created_at) FROM inquiry_to_dbs");
			$qry = $conn->prepare("SELECT COUNT(DATE(created_at)) as days FROM inquiry_to_dbs WHERE created_at LIKE '%$date%'");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
	public static function visaChart($date){
		
		$conn = new ChartDatabase;
		if($conn){
			//$qry = $conn->prepare("SELECT DISTINCT DATE(created_at) FROM inquiry_to_dbs");
			$qry = $conn->prepare("SELECT COUNT(DATE(created_at)) as days FROM visa_process WHERE created_at LIKE '%$date%'");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
	public static function tourChart($date){
		
		$conn = new ChartDatabase;
		if($conn){
			//$qry = $conn->prepare("SELECT DISTINCT DATE(created_at) FROM inquiry_to_dbs");
			$qry = $conn->prepare("SELECT COUNT(DATE(created_at)) as days FROM tour_applications WHERE created_at LIKE '%$date%'");
			$qry->execute();
			$result=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
	}
	
}
?>