<?php
namespace App\Models;

use App\Libraries\Database;
use PDO;

class Login extends Database {
	
	public function log_it_in($user,$pass){
		
		$conn = new Database;
		if ($conn):
		$query=$conn->prepare("Select pass,id from vpcpay_users where username=:user or email = :user");
		$query->execute(array(
			':user'=>$user
			));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		if($result['pass']){
			//check the password and start the session and redirect to dashboard.php.
			if($this->check_password($result['pass'],$pass)){
				$updateUserStats = $conn->prepare('update  vpcpay_users set status = 1 where username=:user or email = :user');
				$updateUserStats->execute(array(':user'=>$user));
				if($this->createSession($result['id'])){
				return true;	
				}
				return false;
			}else{
				return false;
			}
		}
		else{
			return false;
		}
		else:
			echo "No connection";
		endif;
	}

	// function for checking password
	private function check_password($hash, $password) {
	 
	    // first 29 characters include algorithm, cost and salt
	    // let's call it $full_salt
	    $full_salt = substr($hash, 0, 29);
	 
	    // run the hash function on $password
	    $new_hash = crypt($password, $full_salt);
	 
	    // returns true or false
	    return ($hash == $new_hash);
	}

	//after successfully login create a session

	private function createSession($user){
		session_start();
		date_default_timezone_set('Asia/Dubai'); // setting the time zone
		$sID = session_id();
		$_SESSION['user']['theID'] = $sID;
		$_SESSION['user']['whosIN'] = $user;
		$_SESSION['user']['TimeIN'] = time();
		return true;
	}
	
}