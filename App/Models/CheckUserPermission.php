<?php
namespace App\Models;

use App\Libraries\Database;
use \PDO;
class CheckUserPermission extends Database{
	
	private function getPermision($user){
		$query = parent::prepare("SELECT permission from vpcpay_users where id=:id");
		$query->execute(array(':id'=>$user));
		$user = $query->fetch(PDO::FETCH_ASSOC);
		return $user['permission'];
	}
	public function pagePermision($userID,Array $userlist){
		$theuser = $this->getPermision($userID);
		if(!in_array((int)$theuser,$userlist)){
			header('Location:404.php');
		}
	}
	public function visaCapabilities($id){
		$userCap = unserialize($this->getUserCapabilities($id));
		if(is_array($userCap)){
			if((int)$userCap['visaCap'] != 1){
				header('Location:404.php');
			}
		}else{
			header('Location:404.php');
		}
	}
	
	public function thePermission($userID){
		return $this->getPermision($userID);
	}

	private function getUserCapabilities($userID){
		$query = parent::prepare("SELECT user_capability from vpcpay_users where id=:id");
		$query->execute(array(':id'=>$userID));
		$result = $query->fetch(PDO::FETCH_ASSOC); 
		return $result['user_capability'];
	}

	public function userCapabilities($id){
		return $this->getUserCapabilities($id);
	}
}