<?php
namespace App\Models;

use App\Libraries\Database;
use \PDO;

class Notifications extends Database {
	public function notifyTransactions_search($agentid){
        $query = parent::prepare("SELECT tb1.id,tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.trans_time,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 JOIN vpcpay_url_token tb0 ON tb1.token = tb0.token JOIN vpcpay_payment_trans as tb2 ON tb0.id = tb2.tokenID WHERE tb1.agent_id =:id AND tb1.notify_stat IN(1,2) ORDER BY tb1.id DESC");
        $query->execute(array(':id' =>$agentid));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function checkVisaNotification2($clientId){
        $query = parent::prepare("SELECT id,name,visa_type,visa_status,date_updated from  visa_records  WHERE agent_who_request =:id AND notify_stat > :notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>0));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function checkNotification($clientId){
        $query = parent::prepare("SELECT tb1.trans_title,tb1.client_name from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.notify_stat =:notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>1));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function checkVisaNotification($clientId){
        $query = parent::prepare("SELECT id,name,visa_type,visa_status from  visa_records  WHERE agent_who_request =:id AND notify_stat =:notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>1));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function checkNotification2($clientId){
        $query = parent::prepare("SELECT tb2.id,tb1.trans_title,tb1.client_name,tb2.trans_date,tb2.trans_time from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.notify_stat >=:notify ORDER BY tb2.id DESC");
        $query->execute(array(':id'=>$clientId, ':notify'=>1));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function updateNotification($clientId){
        $query = parent::prepare("UPDATE vpcpay_trans_att SET notify_stat = :newNotify WHERE agent_id =:id AND notify_stat =:notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>1, ':newNotify'=>2));
        return 'updated';
    }
    public function updateVisaNotification($clientId){
        $query = parent::prepare("UPDATE visa_records SET notify_stat = :newNotify WHERE agent_who_request =:id AND notify_stat =:notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>1, ':newNotify'=>2));
        return 'updated';
    }
    public function updateVisaNotification2($visaId){
        $query = parent::prepare("UPDATE visa_records SET notify_stat = :newNotify WHERE id =:idVisa");
        $query->execute(array(':idVisa'=>$visaId, ':newNotify'=>0));
        return 'updated';
    }
    public function updateNotification2($notifyId){
        $query = parent::prepare("UPDATE vpcpay_trans_att SET notify_stat = 0 WHERE id =:Nid");
        $query->execute(array(':Nid'=>$notifyId));
        return 'updated';
    }
    public function updateNotification3($notifyId){
        $query = parent::prepare("UPDATE vpcpay_trans_att SET notify_stat = 3 WHERE id =:Nid");
        $query->execute(array(':Nid'=>$notifyId));
        return 'set to default';
    }
    public function visaRequestByID($visaID){
        $query=parent::prepare("SELECT * from visa_records where id =:id AND record_status >= 1");
        $query->execute(array(':id'=>$visaID));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $key => $arrayValues){
            if($arrayValues['date_exit'] !== '0000-00-00'){
                $result[$key]['date_exit'] = $this->conDate($arrayValues['date_exit']);
            }else if($arrayValues['date_exit'] === '0000-00-00'){
                $result[$key]['date_exit'] = 'Not Set';
            }
            if($arrayValues['date_entry'] !== '0000-00-00'){
                $result[$key]['date_entry'] = $this->conDate($arrayValues['date_entry']);
            }else if($arrayValues['date_entry'] === '0000-00-00'){
                $result[$key]['date_entry'] = 'Not Set';
            }
        }
        return $result;
    }
    private function conDate($date){
        $Thedate = new DateTime($date);
        return $Thedate->format('F d, Y');
    }
}