<?php
namespace App\Models;

use App\Libraries\Database;
use \PDO;

class Query extends Database {
	
	// query that show all table content
	public function getTransactions($agentid){
		//$conn = new Database;
		$query = Database::prepare("SELECT tb1.client_name,tb1.status,tb1.date_create,tb1.price,tb1.trans_title,tb1.tokenID,tb2.token,tb2.attempts from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.agent_id = :id ORDER BY tb1.id DESC");
		$query->execute(array(':id'=>$agentid));
		$result=$query->fetchAll();
		return $result;
	}

	public function queryToken($token){
		$query = Database::prepare("SELECT * from vpcpay_url_token");
		$query->execute();
		$result=$query->fetchAll();
		return $result;
	}

	public function getClientTransactionAtt($agentid){
		$query = Database::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id");
		$query->execute(array(':id'=>$agentid));
		return $query->fetchAll();
	}
	
	public function getClientTransactionAttDate($agentid,$date){

		if(!empty($agentid)) {
            $query = Database::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id AND tb1.trans_date = :todayD ORDER BY tb1.id DESC");
            $query->execute(array(':id' => $agentid,
                ':todayD' => $date));

		}else
		    {
			$query = Database::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.trans_date = :todayD  ORDER BY tb1.id DESC");
		    $query->execute(array(':todayD'=>$date));
		}
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	
	public function getClientTransactionAttAgent($agentid){
		$query = Database::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id AND tb1.trans_date = :todayD  ORDER BY tb1.id DESC");
		$query->execute(array(':id'=>$agentid,
							  ':agent'=>'2017-01-30'));
		return $query->fetchAll();
	}

	public function getUserDetails($userID){
		$query = Database::prepare("SELECT full_name,permission,status,email from vpcpay_users WHERE id = :id");
		$query->execute(array(':id'=>$userID));
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	public function delpayRec($id){
		$forTokenTable = Database::prepare("DELETE from vpcpay_url_token WHERE id = :id");
		$forTokenTable->execute(array(':id'=>$id));
		$forTransTable = Database::prepare("DELETE from vpcpay_payment_trans WHERE tokenID = :id");
		$forTransTable->execute(array(':id'=>$id));
		return true;
	}

	public function getAgents(){
		$query = Database::prepare("SELECT * from vpcpay_users WHERE permission = 1");
		$query->execute();
		return $query->fetchAll();
	}

	//Modification (view by Date for payment link)
	public function getTransactions_wDate($agentid,$wDate){
		$query = Database::prepare("SELECT tb1.client_name,tb1.status,tb1.date_create,tb1.price,tb1.trans_title,tb1.tokenID,tb2.token,tb2.attempts from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.agent_id = :id AND tb1.date_create = :fDate ORDER BY tb1.id DESC");
		$query->execute(array(':id'=>$agentid,
			                  ':fDate'=>$wDate));
		$result=$query->fetchAll();
		return $result;
	}

    //Modification (view by search name for payment link)
    public function getTransactions_search($agentid,$search){
        $query = Database::prepare("SELECT tb1.client_name,tb1.status,tb1.date_create,tb1.price,tb1.trans_title,tb1.tokenID,tb2.token,tb2.attempts from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.agent_id = :id AND tb1.client_name like :cName ORDER BY tb1.id DESC");
        $query->execute(array(':id'=>$agentid,
            ':cName'=>'%'.$search.'%'));
        $result=$query->fetchAll();
        return $result;
    }
    //Modification (view by search name for payment link)
    public function getClientTransactionAttSearch($agentid,$search){
        $query = Database::prepare("SELECT tb1.onCard_name,tb1.card_bank,tb1.card_country,tb1.client_ip,tb1.trans_date,tb1.ref_num,tb1.status,tb1.receiptNo,tb2.client_name,tb2.price,tb2.trans_title,tb2.currency from vpcpay_trans_att as tb1 INNER JOIN vpcpay_payment_trans as tb2 ON tb1.ref_num = tb2.refNun WHERE tb1.agent_id =:id AND tb1.onCard_name like :cardName ORDER BY tb1.id DESC");
        $query->execute(array(':id'=>$agentid, ':cardName'=>'%'.$search.'%'));
        return $query->fetchAll();
    }

    //Modification (transaction status SUCCESSFUL count)
    public function transStatusSuccess($agentId){
        $stat = "Transaction Successful";
        $query = Database::prepare("SELECT count(*) as total from vpcpay_trans_att WHERE status = :stat AND agent_id = :agentID");
        $query->execute(array(':agentID'=>$agentId,':stat'=>$stat));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //Modification (transaction status FAILED count)
    public function transStatusFailed($agentId){
        $stat = "Transaction Successful";
        $query = Database::prepare("SELECT count(*) as total from vpcpay_trans_att WHERE status <> :stat AND agent_id = :agentID");
        $query->execute(array(':agentID'=>$agentId,':stat'=>$stat));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //Modification (view by search name for payment link)
    public function clientPaymentLinkForEmail($clientId){
        $query = Database::prepare("SELECT tb1.refNun,tb1.trans_title,tb1.client_name,tb1.price,tb1.client_email,tb1.currency,tb2.token from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_url_token as tb2 ON tb1.tokenID = tb2.id WHERE tb1.tokenID =:id");
        $query->execute(array(':id'=>$clientId));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //Modification (search new notification from email)
    public function checkNotification($clientId){
        $query = Database::prepare("SELECT tb1.trans_title,tb1.client_name from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.notify_stat =:notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>1));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    //Modification (search new notification from email)
    public function updateNotification($clientId){
        $query = Database::prepare("UPDATE vpcpay_trans_att SET notify_stat = :newNotify WHERE agent_id =:id AND notify_stat =:notify");
        $query->execute(array(':id'=>$clientId, ':notify'=>1, ':newNotify'=>0));
        return 'updated';
    }

    //Modification (view by search name for payment link)
    public function populateClient($clientId){
        $query = Database::prepare("SELECT trans_title,client_name,price,client_email,currency from vpcpay_payment_trans  WHERE tokenID =:id");
        $query->execute(array(':id'=>$clientId));
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //For Reports functions
    public function generateAgentReport($agentId,$dateFrom,$dateTo){
        $query0 = Database::prepare("SELECT count(id) from vpcpay_trans_att  WHERE agent_id =:id AND status =:status AND trans_date >=:date1 AND trans_date <=:date2");
        $query1 = Database::prepare("SELECT tb1.price,tb1.currency,tb2.trans_date from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr ORDER BY tb2.trans_date");
        $query2 = Database::prepare("SELECT tb1.price,tb1.currency,tb2.trans_date from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr ORDER BY tb2.trans_date");
        $query3 = Database::prepare("SELECT full_name from vpcpay_users  WHERE id =:id");
        $query0->execute(array(':id'=>$agentId,
            ':status'=>'Transaction Successful',
            ':date1'=>$dateFrom,
            ':date2'=>$dateTo));
        $query1->execute(array(':id'=>$agentId,
            ':status'=>'Transaction Successful',
            ':date1'=>$dateFrom,
            ':date2'=>$dateTo,
            ':curr'=>'aed'));
        $query2->execute(array(':id'=>$agentId,
            ':status'=>'Transaction Successful',
            ':date1'=>$dateFrom,
            ':date2'=>$dateTo,
            ':curr'=>'us'));
        $query3->execute(array(':id'=>$agentId));
        $result0=$query0->fetch(PDO::FETCH_ASSOC);
        $result1=$query1->fetchAll(PDO::FETCH_ASSOC);
        $result2=$query2->fetchAll(PDO::FETCH_ASSOC);
        $result3=$query3->fetch(PDO::FETCH_ASSOC);
        $var = array();
        $var['CNT'] = $result0;
        $var['AED'] = $result1;
        $var['USD'] = $result2;
        $var['AGT'] = $result3;
//        $var = array();
//        array_push($var,$result1);
//        array_push($var,$result2);
        array_merge($var['AED'],$var['USD'],$var['CNT'],$var['AGT']);
        array_merge($var['AED'],$var['USD'],$var['CNT']);
        return($var);
    }
    public function agentDataReport($dateFrom,$dateTo){
        $agentData = '';
        $query = Database::prepare("SELECT id,full_name from vpcpay_users  WHERE permission =:per");
        $query->execute(array(':per'=>'1'));
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $key => $val) {
            $aID = '';
            $amntAED = 0;
            $amntUSD = 0;
            $noClient = 0;
            $name = '';
            foreach($val as $k  => $v){
                if($k == 'id') {
                    $query0 = Database::prepare("SELECT count(id) from vpcpay_trans_att  WHERE agent_id =:id AND status =:status AND trans_date >=:date1 AND trans_date <=:date2");
                    $query0->execute(array(':id'=>$v,
                        ':status'=>'Transaction Successful',
                        ':date1'=>$dateFrom,
                        ':date2'=>$dateTo));
                    $query1 = Database::prepare("SELECT sum(tb1.price) as totalAmount0 from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr");
                    $query1->execute(array(':id'=>$v,
                        ':status'=>'Transaction Successful',
                        ':date1'=>$dateFrom,
                        ':date2'=>$dateTo,
                        ':curr'=>'aed'));
                    $query2 = Database::prepare("SELECT sum(tb1.price) as totalAmount1 from vpcpay_payment_trans as tb1 INNER JOIN vpcpay_trans_att as tb2 ON tb1.refNun = tb2.ref_num  WHERE tb1.agent_id =:id AND tb2.status =:status AND tb2.trans_date >=:date1 AND tb2.trans_date <=:date2 AND tb1.currency=:curr");
                    $query2->execute(array(':id'=>$v,
                        ':status'=>'Transaction Successful',
                        ':date1'=>$dateFrom,
                        ':date2'=>$dateTo,
                        ':curr'=>'us'));
                    $row1 = $query1->fetch(PDO::FETCH_ASSOC);
                    $row2 = $query2->fetch(PDO::FETCH_ASSOC);
                    $row3=$query0->fetchColumn();
                    $amntAED = $row1['totalAmount0'];
                    $amntUSD = $row2['totalAmount1'];
                    $noClient = $row3;
                    $aID = $v;
                }
                if($k == 'full_name') {
                    $name = $v;
                }
            }
            $agentData[$aID]['client'] = $noClient;
            $agentData[$aID]['aed'] = $amntAED;
            $agentData[$aID]['usd'] = $amntUSD;
            $agentData[$aID]['name'] = $name;
        }
        return($agentData);
    }
    //For Reports functions

    public function getRegUser(){
    	$query = Database::prepare('SELECT id,full_name,email,permission from vpcpay_users');
    	$query->execute();
    	return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function changePass($id, $pass){
    	return $this->saveNewPass($id, $pass);
    }

    private function saveNewPass($userID,$newpass){
    	$query = Database::prepare('UPDATE vpcpay_users set pass = :newPAss where id = :userID');
    	$query->execute(array(
    		':newPAss'=>$newpass,
    		':userID'=>$userID
    		));
    	if ($query){
    		return true;
    	}else{
    		return false;
    	}
    }

    private function userCap($id,$capabilities){
    	$query=Database::prepare("UPDATE vpcpay_users set user_capability = :userCap WHERE id = :id");
    	$query->execute(array(
    			':userCap'=>$capabilities,
    			':id'=>$id,
    		));
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function updateUsercap($id,$capabilities){
    	return $this->userCap($id,$capabilities);
    }

    public function getRegUserCap($id){
    	$query = Database::prepare('SELECT user_capability from vpcpay_users where id = :id');
    	$query->execute(array(':id'=>$id));
    	return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function getCountries(){
		$query=Database::prepare("SELECT * from countries");
		$query->execute();
		$result =$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function SingleTable(){
		$query = Database::prepare("SELECT id,email,full_name from vpcpay_users where permission = 1");
		$query->execute();
		$result=$query->fetchAll();
		return $result;
	}
	
}