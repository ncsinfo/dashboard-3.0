<?php
namespace App\Model;

use App\Libraries\Database;
use \PDO;

class Users extends Database {
	
	//Add New User
	public function add($userDetails){
		$query = parent::prepare("Insert into vpcpay_users(full_name,username,email,pass,permission,status) VALUES(:name,:username,:email_add,:pass,:permission,:status)");
		$query->execute(array(
				':name' => $userDetails['name'],
				':username' => $userDetails['username'],
				':email_add' => $userDetails['emailAdd'],
				':pass' => $userDetails['pass'],
				':permission' => $userDetails['userType'],
				':status'=> 1,
			));
		return ($query ? true : false);
	}
	
}